<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/4
 * Time: 10:15
 */

$allowedExts          = array(
	"gif",
	"jpeg",
	"jpg",
	"png"
);
if (isset($_FILES['background'])){
	$background           = $_FILES['background'];
	$temp_background      = explode( ".", $background["name"] );
	$extension_background = end( $temp_background ); // 获取文件后缀名
	if ( ( ( $background["type"] == "image/gif" )
	       || ( $background["type"] == "image/jpeg" )
	       || ( $background["type"] == "image/jpg" )
	       || ( $background["type"] == "image/pjpeg" )
	       || ( $background["type"] == "image/x-png" )
	       || ( $background["type"] == "image/png" ) )
	     && ( $background["size"] < 1024 * 4 )   // 小于 4M
	     && in_array( $extension_background, $allowedExts )){
		if ( $background["error"] > 0 ) {
			exit("错误：: " . $background["error"] . "<br />");
		} else {
			$bgPath = "background" . '.' . "png";
			$qrPath = "qrcode" . "." . "png";
			move_uploaded_file( $background["tmp_name"], "upload/" . $bgPath );
			exit('success' . "upload/" . $bgPath);
		}
	} else {
		exit("非法的文件格式");
	}
}
//$background           = $_FILES['background'];
//$qrCode               = $_FILES['QRCode'];

//$temp_background      = explode( ".", $background["name"] );
//$temp_qrCode          = explode( ".", $qrCode["name"] );
//$extension_background = end( $temp_background ); // 获取文件后缀名
//$extension_qrCode     = end( $temp_qrCode ); // 获取文件后缀名

if ( ( ( $background["type"] == "image/gif" )
       || ( $background["type"] == "image/jpeg" )
       || ( $background["type"] == "image/jpg" )
       || ( $background["type"] == "image/pjpeg" )
       || ( $background["type"] == "image/x-png" )
       || ( $background["type"] == "image/png" ) )
     && ( $background["size"] < 1024 * 4 )   // 小于 4M
     && in_array( $extension_background, $allowedExts )
     || ( ( $qrCode["type"] == "image/gif" )
          || ( $qrCode["type"] == "image/jpeg" )
          || ( $qrCode["type"] == "image/jpg" )
          || ( $qrCode["type"] == "image/pjpeg" )
          || ( $qrCode["type"] == "image/x-png" )
          || ( $qrCode["type"] == "image/png" ) )
        && ( $qrCode["size"] < 1024 * 4 )   // 小于 4M
        && in_array( $extension_qrCode, $allowedExts ) ) {
	if ( $background["error"] > 0 ) {
		exit("错误：: " . $background["error"] . $qrCode["error"] . "<br>");
	} else {
//		echo "上传文件名: " . $background["name"] . "<br>";
//		echo "文件类型: " . $background["type"] . "<br>";
//		echo "文件大小: " . ( $background["size"] / 1024 ) . " kB<br>";
//		echo "文件临时存储的位置: " . $background["tmp_name"] . "<br>";
//		echo "上传文件名: " . $qrCode["name"] . "<br>";
//		echo "文件类型: " . $qrCode["type"] . "<br>";
//		echo "文件大小: " . ( $qrCode["size"] / 1024 ) . " kB<br>";
//		echo "文件临时存储的位置: " . $qrCode["tmp_name"] . "<br>";
		$bgPath = "background" . '.' . "png";
		$qrPath = "qrcode" . "." . "png";
		move_uploaded_file( $background["tmp_name"], "upload/" . $bgPath );
		move_uploaded_file( $qrCode["tmp_name"], "upload/" . $qrPath );
		exit('success');

		$bg_url = "upload/" . $bgPath;
		$qr_url = "upload/" . $qrPath;
		echo "文件存储在: " . "upload/" . $bgPath ."upload/" . $qrPath;

		$save_url = "upload/" . $bgPath;

		$file_path = $save_url;
		$width     = 96;
		$height    = 96;
		$size      = getimagesize( $file_path );
		if ( $size[2] == 1 ) {
			$im_in = imagecreatefromgif( $file_path );
		}
		if ( $size[2] == 2 ) {
			$im_in = imagecreatefromjpeg( $file_path );
		}
		if ( $size[2] == 3 ) {
			$im_in = imagecreatefrompng( $file_path );
		}
		$im_out = imagecreatetruecolor( $width, $height );
		imagecopyresampled( $im_out, $im_in, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
		imagejpeg( $im_out, $file_path );
		imagedestroy( $im_in );
		imagedestroy( $im_out );
	}
} else {
	exit("非法的文件格式");
}