<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/5
 * Time: 9:35
 */


session_start();

$allowedExts = array(
	"gif",
	"jpeg",
	"jpg",
	"png"
);

if ( isset( $_FILES["background"] ) ) {
	$bg_file = $_FILES["background"];
	$bg_name = $bg_file["name"];
	$bg_size = $bg_file["size"];
	if ( $bg_name != "" ) {
		if ( $bg_file["error"] ) {
			exit( '{"status":0,"content":"' . $bg_file["error"] . '"}' );
		}
		if ( $bg_size > ( round( 1024 * 1024, 2 ) * 12 ) ) { //限制上传大小
			exit( '{"status":0,"content":"图片大小不能超过12M"}' );
		}
		$type               = strstr( $bg_name, "." ); //限制上传格式
		$temp_bg_name_array = explode( ".", $bg_name );
		$ff_bg              = end( $temp_bg_name_array );
		if ( ! in_array( $ff_bg, $allowedExts ) ) {
			exit( '{"status":2,"content":"图片格式不对！"}' );
		}
		$bg_filename = "background." . $ff_bg; //命名图片名称
		//上传路径
		$bg_path        = "upload/" . $bg_filename;
		$_SESSION['bg'] = $bg_path;
		$_SESSION['bgcut'] = 0;
		$_SESSION['bgcha'] = 0;
		move_uploaded_file( $bg_file["tmp_name"], $bg_path );
		$size_bg    = round( $bg_size / 1024, 2 ); //转换成kb
		$bg_im_size = getimagesize( $bg_path );
		exit( '{"status":1,"url":"' . $bg_path . '","width":"' . $bg_im_size[0] . '","height":"' . $bg_im_size[1] . '","size":"' . $size_bg . '"}' );
	}
}
if ( isset( $_FILES["QRCode"] ) ) {
	$qr_file = $_FILES["QRCode"];
	$qr_name = $qr_file["name"];
	$qr_size = $qr_file["size"];
	if ( $qr_name != "" ) {
		if ( $qr_file["error"] ) {
			exit( '{"status":0,"content":"' . $qr_file["error"] . '"}' );
		}
		if ( $qr_size > ( round( 1024 * 1024, 2 ) * 12 ) ) { //限制上传大小
			exit( '{"status":0,"content":"图片大小不能超过12M"}' );
		}
		$type               = strstr( $qr_name, "." ); //限制上传格式
		$temp_qr_name_array = explode( ".", $qr_name );
		$ff_qr              = end( $temp_qr_name_array );
		if ( ! in_array( $ff_qr, $allowedExts ) ) {
			exit( '{"status":2,"content":"图片格式不对！"}' );
		}
		$qr_filename = "qrcode." . $ff_qr; //命名图片名称
		//上传路径
		$qr_path        = "upload/" . $qr_filename;
		$_SESSION['qr'] = $qr_path;
		$_SESSION['qrcut'] = 0;
		$_SESSION['qrcha'] = 0;
		move_uploaded_file( $qr_file["tmp_name"], $qr_path );
		$size_qr = round( $qr_size / 1024, 2 ); //转换成kb
		$qr_im_size = getimagesize( $qr_path );
		exit( '{"status":1,"url":"' . $qr_path . '","width":"' . $qr_im_size[0] . '","height":"' . $qr_im_size[1] . '","size":"' . $size_qr . '"}' );
	}
}