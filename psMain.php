<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/3
 * Time: 16:45
 */


require_once "phpqrcode/phpqrcode.php";

?>


<!--<body background="images/photoPng.jpg">-->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
<script src="js/app.js"></script>
<body background="images/bg.jpeg">
<div id="createImage" style="background: #F0F8FB">


    新建画布：
    <br/>
    宽：<input type="text" id="imageWidth"/>
    <br/>
    高：<input type="text" id="imageHeight"/>
    <br/>
    <input type="button" onclick="create_new_bg()" value="新建"/>

    <hr/>
    <div id="upload-img">
        <form id="upload-bg" action="upload.php" method="post" enctype="multipart/form-data">

            背景图片：<input type="file" id="background" name="background"/>
        </form>
        <div class="bg-list"></div>
        <div class="bg-res"></div>
        <div class="bg-progress">
            <div class="bg-progress-Bar">
                <span class="bg-percent"></span>
            </div>
        </div>
        宽：<input class="x1" value="0" disabled="disabled"/><br/>
        高：<input class="y1" value="0" disabled="disabled"/><br/>
        -------------------------------------<br/>
        <span style="color: #0066FF;">背景编辑：</span><br/>
        背景背景颜色设置：<input id="background-color" type="color" value="#FFFFFF"/><br/>
        背景图片透明度：<input style="width: 100px" id="pct-bg" type="text" value="100"/>%<br/>
        <span id="cut1"><input class="cut1" onclick="cut_bg()" type="button" value="背景裁剪"/></span>
        <span id="cha1"><input class="cha1" type="button" onclick="cha_bg()" value="背景缩放"/></span>

        <div id="change-bg"></div>
        <div class="bg-list-change"></div>
        <hr/>


        <form id="upload-qr" action="upload.php" method="post" enctype="multipart/form-data">
            二维码：<input type="file" id="QRCode" name="QRCode""/>
        </form>
        普通二维码手动生成：<br/>
        <input id="makeqrtext" type="text"/><input id="makeqr" onclick="" type="button" value="make"/>
        <div class="qr-list"></div>
        <div class="qr-res"></div>
        <div class="qr-progress">
            <div class="qr-progress-Bar">
                <span class="qr-percent"></span>
            </div>
        </div>
        宽：<input class="x2" value="0" disabled="disabled"/><br/>
        高：<input class="y2" value="0" disabled="disabled"/><br/>
        -------------------------------------<br/>
        <span style="color: #0066FF;">二维码编辑：</span><br/>
        二维码背景颜色设置：<input id="qr-color" type="color" value="#FFFFFF"/><br/>
        二维码图片透明度：<input style="width: 100px" id="pct-qr" type="text" value="100"/>%<br/>
        <span id="cut2"><input class="cut2" type="button" onclick="cut_qr()" value="二维码裁剪"/></span>
        <span id="cha2"><input class="cha2" type="button" onclick="cha_qr()" value="二维码缩放"/></span>
        <div id="change-qr"></div>
        <div class="qr-list-change"></div>
        <hr/>
        二维码移动到：
        <br/>
        偏移x：<input type="text" id="mx" value="0"/><br/>
        偏移y：<input type="text" id="my" value="0"/><br/>
        <input class="lt" type="button" onclick="" value="左上角"/>
        <input class="ct" type="button" onclick="" value="上中央"/>
        <input class="rt" type="button" onclick="" value="右上角"/><br/>
        <input class="ld" type="button" onclick="" value="左下角"/>
        <input class="cd" type="button" onclick="" value="下中央"/>
        <input class="rd" type="button" onclick="" value="右下角"/><br/>
        <hr/>
        二维码组合透明度：<input style="width: 100px" id="pct-ps" type="text" value="100"/>%<br/>
        <input type="button" onclick="psnow()" value="组合"/>
        预览：

    </div>
</div>
<br/>
<input type="button"
       onclick="var bg_width = $('#outbg').width(); var qr_width = $('#outqr').width(); var bg_height = $('#outbg').height(); var qr_height = $('#outqr').height(); alert(bg_width+'|'+qr_width+'|'+bg_height+'|'+qr_height);"
       value="123"/>
<div id="preview"></div>

<br/>
<hr/>
<img src="psTest.php" />

<script type="text/javascript">
    $(document).ready(function (e) {
        var bg_progress = $(".bg-progress");
        var bg_progress_bar = $(".bg-progress-Bar");
        var bg_percent = $(".bg-percent");
        $("#background").change(function () {
            $("#upload-bg").ajaxSubmit({
                dataType: "json", //数据格式为json
                beforeSend: function () { //开始上传
                    bg_progress.show();
                    var percentVal = "0%";
                    bg_progress_bar.width(percentVal);
                    bg_percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + "%"; //获得进度
                    bg_progress_bar.width(percentVal); //上传进度条宽度变宽
                    bg_percent.html(percentVal); //显示上传进度百分比
                },
                success: function (data) {
                    if (data.status == 1) {
                        var src = data.url;
                        var attstr = "<img src='" + src + "?r=" + Math.random() + "'>";
                        $(".bg-list").html(attstr);
                        $(".bg-res").html("图片大小：" + data.size + "K");
                        $(".x1").val(data.width);
                        $(".y1").val(data.height);

                        $("#change-bg").html("");
                        $(".cha1").css("color", "#000000");
                        $(".cut1").css("color", "#000000");
                    } else {
                        $(".bg-res").html(data.content);
                    }
                    bg_progress.hide();
                },
                error: function (xhr, tS, rT) { //上传失败
                    alert("上传失败");
                    bg_progress.hide();
                }
            });
        });
        var qr_progress = $(".qr-progress");
        var qr_progress_bar = $(".qr-progress-Bar");
        var qr_percent = $(".qr-percent");
        $("#QRCode").change(function () {
            $("#upload-qr").ajaxSubmit({
                dataType: "json", //数据格式为json
                beforeSend: function () { //开始上传
                    qr_progress.show();
                    var percentVal = "0%";
                    qr_progress_bar.width(percentVal);
                    qr_percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + "%"; //获得进度
                    qr_progress_bar.width(percentVal); //上传进度条宽度变宽
                    qr_percent.html(percentVal); //显示上传进度百分比
                },
                success: function (data) {
                    if (data.status == 1) {
                        var src = data.url;
                        var attstr = "<img src='" + src + "?r=" + Math.random() + "'>";
                        $(".qr-list").html(attstr);
                        $(".qr-res").html("图片大小：" + data.size + "K");
                        $(".x2").val(data.width);
                        $(".y2").val(data.height);

                        $("#change-bg").html("");
                        $(".cha1").css("color", "#000000");
                        $(".cut1").css("color", "#000000");
                    } else {
                        $(".qr-res").html(data.content);
                    }
                    qr_progress.hide();
                },
                error: function (xhr) { //上传失败
                    alert("上传失败");
                    qr_progress.hide();
                }
            });
        });
        $(".lt").click(function () {
            $("#mx").val(0);
            $("#my").val(0);
        });
        $(".ct").click(function () {
            var bg_width = $("#outbg").width();
            var qr_width = $("#outqr").width();

            if (isNaN(bg_width) && isNaN(qr_width)) {
                var x = $(".x1").val() / 2 - $(".x2").val() / 2;
                $("#mx").val(x);
                $("#my").val(0);
            }
            else if (!isNaN(bg_width) && isNaN(qr_width)) {
                var x = bg_width / 2 - $(".x2").val() / 2;
                $("#mx").val(x);
                $("#my").val(0);
            } else if (isNaN(bg_width) && !isNaN(qr_width)) {
                var x = $(".x1").val() / 2 - qr_width / 2;
                $("#mx").val(x);
                $("#my").val(0);
            } else {
                var x = bg_width / 2 - qr_width / 2;
                $("#mx").val(x);
                $("#my").val(0);
            }
        });
        $(".rt").click(function () {
            var bg_width = $("#outbg").width();
            var qr_width = $("#outqr").width();

            if (isNaN(bg_width) && isNaN(qr_width)) {
                var x = $(".x1").val() - $(".x2").val();
                $("#mx").val(x);
                $("#my").val(0);
            }
            else if (!isNaN(bg_width) && isNaN(qr_width)) {
                var x = bg_width - $(".x2").val();
                $("#mx").val(x);
                $("#my").val(0);
            } else if (isNaN(bg_width) && !isNaN(qr_width)) {
                var x = $(".x1").val() - qr_width;
                $("#mx").val(x);
                $("#my").val(0);
            } else {
                var x = bg_width - qr_width;
                $("#mx").val(x);
                $("#my").val(0);
            }
        });
        $(".ld").click(function () {
            var bg_height = $("#outbg").height();
            var qr_height = $("#outqr").height();

            if (isNaN(bg_height) && isNaN(qr_height)) {
                var y = $(".y1").val() - $(".y2").val();
                $("#mx").val(0);
                $("#my").val(y);
            } else if (!isNaN(bg_height) && isNaN(qr_height)) {
                var y = bg_height - $(".y2").val();
                $("#mx").val(0);
                $("#my").val(y);
            } else if (isNaN(bg_height) && !isNaN(qr_height)) {
                var y = $(".y1").val() - qr_height;
                $("#mx").val(0);
                $("#my").val(y);
            } else {
                var y = bg_height - qr_height;
                $("#mx").val(0);
                $("#my").val(y);
            }
        });
        $(".cd").click(function () {
            var bg_width = $("#outbg").width();
            var qr_width = $("#outqr").width();
            var bg_height = $("#outbg").height();
            var qr_height = $("#outqr").height();

            if (isNaN(bg_width) && isNaN(qr_width)) {
                var x = $(".x1").val() / 2 - $(".x2").val() / 2;
                var y = $(".y1").val() - $(".y2").val();
                $("#mx").val(x);
                $("#my").val(y);
            } else if (!isNaN(bg_width) && isNaN(qr_width)) {
                var x = bg_width / 2 - $(".x2").val() / 2;
                var y = bg_height - $(".y2").val();
                $("#mx").val(x);
                $("#my").val(y);
            } else if (isNaN(bg_width) && !isNaN(qr_width)) {
                var x = $(".x1").val() / 2 - qr_width / 2;
                var y = $(".y1").val() - qr_height;
                $("#mx").val(x);
                $("#my").val(y);
            } else {
                var x = bg_width / 2 - qr_width / 2;
                var y = bg_height - qr_height;
                $("#mx").val(x);
                $("#my").val(y);
            }
        });
        $(".rd").click(function () {
            var bg_width = $("#outbg").width();
            var qr_width = $("#outqr").width();
            var bg_height = $("#outbg").height();
            var qr_height = $("#outqr").height();

            if (isNaN(bg_width) && isNaN(qr_width)) {
                var x = $(".x1").val() - $(".x2").val();
                var y = $(".y1").val() - $(".y2").val();
                $("#mx").val(x);
                $("#my").val(y);
            } else if (!isNaN(bg_width) && isNaN(qr_width)) {
                var x = bg_width - $(".x2").val();
                var y = bg_height - $(".y2").val();
                $("#mx").val(x);
                $("#my").val(y);
            } else if (isNaN(bg_width) && !isNaN(qr_width)) {
                var x = $(".x1").val() - qr_width;
                var y = $(".y1").val() - qr_height;
                $("#mx").val(x);
                $("#my").val(y);
            } else {
                var x = bg_width - qr_width;
                var y = bg_height - qr_height;
                $("#mx").val(x);
                $("#my").val(y);
            }
        });
    })
    ;
</script>
</body>












