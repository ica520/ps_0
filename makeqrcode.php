<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/6
 * Time: 17:01
 */


require_once 'phpqrcode/phpqrcode.php';


if ( isset( $_POST['makeCode'] ) ) {
	$string = $_POST['makeCode'];
	QRcode::png( $string, 'images/qrcode.png', QR_ECLEVEL_L, 20, 4, false );
}

//$string = 'FAQ';
$string = 'https://mp.weixin.qq.com/s/z7hRSJfLxY_6Q6Rt_rbEzA';
QRcode::png( $string, 'images/qrcode.png', 3, 3, 4, false );

list( $qrcode_width, $qrcode_height ) = getimagesize( 'images/qrcode.png' );
list( $logo_width, $logo_height ) = getimagesize( 'realPs/images/logooo.png' );
$image_qrcode = imagecreatefrompng( 'images/qrcode.png' );
$image_logo   = imagecreatefromjpeg( 'realPs/images/logooo.png' );
$qrcode_bg    = imagecreatetruecolor( 201, 201 );
$white        = imagecolorallocatealpha( $qrcode_bg, 255, 255, 255, 0 );
$transparent  = imagecolorallocatealpha( $qrcode_bg, 0, 0, 0, 127 );
imagefill( $qrcode_bg, 0, 0, $transparent );
imagecopyresampled( $qrcode_bg, $image_qrcode, 0, 0, 0, 0, 201, 201, $qrcode_width, $qrcode_height );
$logo_bg = imagecreate( 45, 45 );
imagefill( $logo_bg, 0, 0, $white );
imagecopyresampled( $logo_bg, $image_logo, 0, 0, 0, 0, 45, 45, $logo_width, $logo_height );
imagecopy( $qrcode_bg, $logo_bg, ( 201 / 2 - 45 / 2 ), ( 201 / 2 - 45 / 2 ), 0, 0, 45, 45 );

header('Content-Type: image/png');
imagepng($qrcode_bg, './realPs/images/qrcode2.png');
imagepng($qrcode_bg);
imagedestroy($qrcode_bg);
?>
<!--<img src="images/qrcode.png"/>-->
