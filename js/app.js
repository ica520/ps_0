function create_new_bg() {
    var imageWidth = $("#imageWidth").val();
    var imageHeight = $("#imageHeight").val();
    if (imageWidth != null && imageWidth != "" && imageHeight != null && imageHeight != "") {
        imageWidth = parseInt(imageWidth);
        imageHeight = parseInt(imageHeight);
        if (isNaN(imageWidth) || isNaN(imageHeight)) {
            alert("请输入数字！");
        } else if (imageWidth < 0 || imageWidth > 15768000 || imageHeight < 0 || imageHeight > 15768000) {
            alert("请输入正确的数字");
        } else {
            $("#preview").html("<img src='ps1.php?width=" +
                imageWidth
                + "&height=" +
                imageHeight
                + "'>");
        }
    }
}

function psnow() {
    var mx = $("#mx").val();
    var my = $("#my").val();
    var pct = $("#pct-ps").val();
    if (mx != null && mx != "" && my != null && my != "") {
        mx = parseInt(mx);
        my = parseInt(my);
        if (isNaN(mx) || isNaN(my))
            alert("请输入正确的数字");
        else
            $("#preview").html("<img src='ps2.php?&mx=" + mx + "&my=" + my + "&pct=" + pct + "&r=" + Math.random() + "'>");
    } else
        alert("请输入正确的数字");
}


////背景编辑

//点击背景裁剪
function cut_bg() {
    var crx = $(".x1").val();
    var cry = $(".y1").val();
    var charx1 = $("#charx1").val();
    var chary1 = $("#chary1").val();

    if (charx1 == "" || charx1 == null || isNaN(charx1)) {
        var html = "背景裁剪：<br/>左上角x：<input type='text' id='cutlx1' onchange='change_cutlx1()' value='0' /><br />左上角y：<input type='text' id='cutly1' onchange='change_cutly1()' value='0' /><br />宽度：<input type='text' id='cutrx1' value='" + crx + "'/><label class='xcut1'></label><br />高度：<input type='text' id='cutry1' value='" + cry + "'/><label class='ycut1'></label><br /><input type='button' id='cut-bg' onclick='cut_bg_post()' value='裁剪'/><input type='button' id='cut-bg-back' onclick='bg_cut_back()' value='取消裁剪'/><br/>";
    }else {
        var html = "背景裁剪：<br/>左上角x：<input type='text' id='cutlx1' onchange='change_cutlx1()' value='0' /><br />左上角y：<input type='text' id='cutly1' onchange='change_cutly1()' value='0' /><br />宽度：<input type='text' id='cutrx1' value='" + charx1 + "'/><label class='xcut1'></label><br />高度：<input type='text' id='cutry1' value='" + chary1 + "'/><label class='ycut1'></label><br /><input type='button' id='cut-bg' onclick='cut_bg_post()' value='裁剪'/><input type='button' id='cut-bg-back' onclick='bg_cut_back()' value='取消裁剪'/><br/>";
    }
    $("#cut1").html(html);
}

//取消裁剪
function bg_cut_back() {
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            bgChangeCutOff: 0
        },
        success: function (data, textStatus, jqXHR) {
            $("#cut1").html("<input class='cut1' onclick='cut_bg()' type='button' value='背景裁剪'/>");
            $(".bg-list-change").html("");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

//改变裁剪值时
function change_cutlx1() {
    var charx1 = $("#charx1").val();

    var cutlx1 = $("#cutlx1").val();
    var crx = $(".x1").val();
    if (charx1 == "" || charx1 == null || isNaN(charx1)) {
        var x = crx - cutlx1;
        $(".xcut1").html(x);
    } else {
        var x = charx1 - cutlx1;
        $(".xcut1").html(x);
    }
}

function change_cutly1() {
    var chary1 = $("#chary1").val();

    var cutly1 = $("#cutly1").val();
    var cry = $(".y1").val();
    if (chary1 == "" || chary1 == null || isNaN(chary1)) {
        var y = cry - cutly1;
        $(".ycut1").html(y);
    }else {
        var y = chary1 - cutly1;
        $(".ycut1").html(y);
    }
}

//裁剪表单发送
function cut_bg_post() {
    var cutlx1 = $("#cutlx1").val();
    var cutly1 = $("#cutly1").val();
    var cutrx1 = $("#cutrx1").val();
    var cutry1 = $("#cutry1").val();
    var bg_color = $("#background-color").val();
    var bg_pct = $("#pct-bg").val();
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "cutbg",
            cutlx1: cutlx1,
            cutly1: cutly1,
            cutrx1: cutrx1,
            cutry1: cutry1,
            bg_color: bg_color,
            bg_pct: bg_pct
        },
        success: function (data, textStatus, jqXHR) {
            $(".bg-list-change").html("<img id='outbg' src='upload/changecut/background.png?r=" + Math.random() + "' width='"+data.width+"' height='"+data.height+"'>");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });

}


//点击背景缩放
function cha_bg() {
    var crx = $(".x1").val();
    var cry = $(".y1").val();
    var cutrx1 = $("#cutrx1").val();
    var cutry1 = $("#cutry1").val();
    if (cutrx1 == "" || cutrx1 == null || isNaN(cutrx1)) {
        var html = "背景缩放：<br/>等比缩放<input id='bg-cha-set' type='checkbox' checked='checked'/><br />宽度：<input type='text' onchange='cha_bg_w()' id='charx1' value='" + crx + "'/><br />高度：<input type='text' onchange='cha_bg_h()' id='chary1' value='" + cry + "'/><br /><input type='button' id='cha-bg' onclick='cha_bg_post()' value='缩放'/><input type='button' id='cha-bg-back' onclick='bg_cha_back()' value='取消缩放'/>";
    }else {
        var html = "背景缩放：<br/>等比缩放<input id='bg-cha-set' type='checkbox' checked='checked'/><br />宽度：<input type='text' onchange='cha_bg_w()' id='charx1' value='" + cutrx1 + "'/><br />高度：<input type='text' onchange='cha_bg_h()' id='chary1' value='" + cutry1 + "'/><br /><input type='button' id='cha-bg' onclick='cha_bg_post()' value='缩放'/><input type='button' id='cha-bg-back' onclick='bg_cha_back()' value='取消缩放'/>";
    }

    $("#cha1").html(html);
}

//取消缩放
function bg_cha_back() {
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            bgChangeChaOff: 0
        },
        success: function (data, textStatus, jqXHR) {
            $("#cha1").html("<input class='cha1' type='button' onclick='cha_bg()' value='背景缩放'/>");
            $(".bg-list-change").html("");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

//缩放宽
function cha_bg_w() {
    var crx = $(".x1").val();
    var cry = $(".y1").val();
    var charx1 = $("#charx1").val();

    var cutrx1 = $("#cutrx1").val();
    var cutry1 = $("#cutry1").val();
    if (cutrx1 == "" || cutrx1 == null || isNaN(cutrx1)) {
        var setvalue = cry / crx;
        if ($("#bg-cha-set").is(":checked")) {
            $("#chary1").val(setvalue * charx1);
        }
    }else {
        var setvalue = cutry1 / cutrx1;
        if ($("#bg-cha-set").is(":checked")) {
            $("#chary1").val(setvalue * charx1);
        }
    }
}

function cha_bg_h() {
    var crx = $(".x1").val();
    var cry = $(".y1").val();
    var chary1 = $("#chary1").val();

    var cutrx1 = $("#cutrx1").val();
    var cutry1 = $("#cutry1").val();
    if (cutrx1 == "" || cutrx1 == null || isNaN(cutrx1)) {
        if ($("#bg-cha-set").is(":checked")) {
            var setvalue = crx / cry;
            $("#charx1").val(setvalue * chary1);
        }
    }else {
        if ($("#bg-cha-set").is(":checked")) {
            var setvalue = cutrx1 / cutry1;
            $("#charx1").val(setvalue * chary1);
        }
    }
}

//背景缩放表单发送
function cha_bg_post() {
    var charx1 = $("#charx1").val();
    var chary1 = $("#chary1").val();
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            type: "chabg",
            charx1: charx1,
            chary1: chary1
        },
        success: function (data, textStatus, jqXHR) {
            $(".bg-list-change").html("<img id='outbg' src='upload/changecha/background.png?r=" + Math.random() + "'>");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}


////二维码编辑

//点击二维码裁剪
function cut_qr() {
    var crx = $(".x2").val();
    var cry = $(".y2").val();
    var charx2 = $("#charx2").val();
    var chary2 = $("#chary2").val();
    if (charx2 == "" || charx2 == null || isNaN(charx2)) {
        var html = "二维码裁剪:<br/>左上角x：<input type='text' id='cutlx2' onchange='change_cutlx2()' value='0' /><br />左上角y：<input type='text' id='cutly2' onchange='change_cutly2()' value='0' /><br />宽度：<input type='text' id='cutrx2' value='" + crx + "'/><label class='xcut2'></label><br />高度：<input type='text' id='cutry2' value='" + cry + "'/><label class='ycut2'></label><br /><input type='button' id='cut-qr' onclick='cut_qr_post()' value='裁剪'/><input type='button' id='cut-qr-back' onclick='qr_cut_back()' value='取消编辑'/><br/>";
    }else{
        var html = "二维码裁剪:<br/>左上角x：<input type='text' id='cutlx2' onchange='change_cutlx2()' value='0' /><br />左上角y：<input type='text' id='cutly2' onchange='change_cutly2()' value='0' /><br />宽度：<input type='text' id='cutrx2' value='" + charx2 + "'/><label class='xcut2'></label><br />高度：<input type='text' id='cutry2' value='" + chary2 + "'/><label class='ycut2'></label><br /><input type='button' id='cut-qr' onclick='cut_qr_post()' value='裁剪'/><input type='button' id='cut-qr-back' onclick='qr_cut_back()' value='取消裁剪'/><br/>";
    }

    $("#cut2").html(html);

}

//取消裁剪
function qr_cut_back() {
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            qrChangeChrOff: 0
        },
        success: function (data, textStatus, jqXHR) {
            $("#cut2").html("<input class='cut2' onclick='cut_qr()' type='button' value='二维码裁剪'/>");
            $(".qr-list-change").html("");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

//改变裁剪值时
function change_cutlx2() {
    var charx2 = $("#charx2").val();

    var cutlx2 = $("#cutlx2").val();
    var crx = $(".x2").val();
    if (charx2 == "" || charx2 == null || isNaN(charx2)) {
        var x = crx - cutlx2;
        $(".xcut2").html(x);
    } else {
        var x = charx2 - cutlx2;
        $(".xcut2").html(x);
    }
}

function change_cutly2() {
    var chary2 = $("#chary2").val();

    var cutly2 = $("#cutly2").val();
    var cry = $(".y2").val();
    if (chary2 == "" || chary2 == null || isNaN(chary2)) {
        var y = cry - cutly2;
        $(".ycut2").html(y);
    }else {
        var y = chary2 - cutly2;
        $(".ycut2").html(y);
    }
}

//裁剪表单发送
function cut_qr_post() {
    var cutlx2 = $("#cutlx2").val();
    var cutly2 = $("#cutly2").val();
    var cutrx2 = $("#cutrx2").val();
    var cutry2 = $("#cutry2").val();
    var qr_color = $("#qr-color").val();
    var qr_pct = $("#pct-qr").val();
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            type: "cutqr",
            cutlx2: cutlx2,
            cutly2: cutly2,
            cutrx2: cutrx2,
            cutry2: cutry2,
            qr_color: qr_color,
            qr_pct: qr_pct
        },
        success: function (data, textStatus, jqXHR) {
            $(".qr-list-change").html("<img id='outqr' src='upload/changecut/qrcode.png?r=" + Math.random() + "'>");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });

}


//点击二维码缩放
function cha_qr() {
    var crx = $(".x2").val();
    var cry = $(".y2").val();
    var cutrx2 = $("#cutrx2").val();
    var cutry2 = $("#cutry2").val();
    if (cutrx2 == "" || cutrx2 == null || isNaN(cutrx2)) {
        var html = "二维码缩放：<br/>等比缩放<input id='qr-cha-set' type='checkbox' checked='checked'/><br />宽度：<input type='text' onchange='cha_qr_w()' id='charx2' value='" + crx + "'/><br />高度：<input type='text' onchange='cha_qr_h()' id='chary2' value='" + cry + "'/><br /><input type='button' id='cha-qr' onclick='cha_qr_post()' value='缩放'/><input type='button' id='cha-qr-back' onclick='qr_cha_back()' value='取消缩放'/>";
    }else {
        var html = "二维码缩放：<br/>等比缩放<input id='qr-cha-set' type='checkbox' checked='checked'/><br />宽度：<input type='text' onchange='cha_qr_w()' id='charx2' value='" + cutrx2 + "'/><br />高度：<input type='text' onchange='cha_qr_h()' id='chary2' value='" + cutry2 + "'/><br /><input type='button' id='cha-qr' onclick='cha_qr_post()' value='缩放'/><input type='button' id='cha-qr-back' onclick='qr_cha_back()' value='取消缩放'/>";
    }

    $("#cha2").html(html);
}

//取消缩放
function qr_cha_back() {
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            qrChangeChaOff: 0
        },
        success: function (data, textStatus, jqXHR) {
            $("#cha2").html("<input class='cha2' onclick='cha_qr()' type='button' value='二维码裁剪'/>");
            $(".qr-list-change").html("");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

//缩放宽
function cha_qr_w() {
    var crx = $(".x2").val();
    var cry = $(".y2").val();
    var charx2 = $("#charx2").val();

    var cutrx2 = $("#cutrx2").val();
    var cutry2 = $("#cutry2").val();
    if (cutrx2 == "" || cutrx2 == null || isNaN(cutrx2)) {
        var setvalue = cry / crx;
        if ($("#qr-cha-set").is(":checked")) {
            $("#chary2").val(setvalue * charx2);
        }
    }else {
        var setvalue = cutry2 / cutrx2;
        if ($("#qr-cha-set").is(":checked")) {
            $("#chary2").val(setvalue * charx2);
        }
    }
}

function cha_qr_h() {
    var crx = $(".x2").val();
    var cry = $(".y2").val();
    var chary2 = $("#chary2").val();

    var cutrx2 = $("#cutrx2").val();
    var cutry2 = $("#cutry2").val();
    if (cutrx2 == "" || cutrx2 == null || isNaN(cutrx2)) {
        if ($("#qr-cha-set").is(":checked")) {
            var setvalue = crx / cry;
            $("#charx2").val(setvalue * chary2);
        }
    }else {
        if ($("#qr-cha-set").is(":checked")) {
            var setvalue = cutrx2 / cutry2;
            $("#charx2").val(setvalue * chary2);
        }
    }
}

//背景缩放表单发送
function cha_qr_post() {
    var charx2 = $("#charx2").val();
    var chary2 = $("#chary2").val();
    $.ajax({
        url: "changeimage.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "html",
        data: {
            type: "chaqr",
            charx2: charx2,
            chary2: chary2
        },
        success: function (data, textStatus, jqXHR) {
            $(".qr-list-change").html("<img id='outqr' src='upload/changecha/qrcode.png?r=" + Math.random() + "'>");
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}