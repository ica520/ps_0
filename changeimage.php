<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/5
 * Time: 17:07
 */


session_start();

if ( isset( $_POST['bgChangeCutOff'] ) ) {
	$_SESSION['bgcut'] = 0;
}
if ( isset( $_POST['bgChangeChaOff'] ) ) {
	$_SESSION['bgcha'] = 0;
}
if ( isset( $_POST['qrChangeCutOff'] ) ) {
	$_SESSION['qrcut'] = 0;
}
if ( isset( $_POST['qrChangeChaOff'] ) ) {
	$_SESSION['qrcha'] = 0;
}
if ( isset( $_POST['type'] ) && isset( $_POST['cutlx1'] ) && isset( $_POST['cutly1'] ) && isset( $_POST['cutrx1'] ) && isset( $_POST['cutry1'] ) && isset( $_POST['bg_color'] ) && isset( $_POST['bg_pct'] ) ) {
	$type     = $_POST['type'];
	$src_x    = $_POST['cutlx1'];
	$src_y    = $_POST['cutly1'];
	$src_w    = $_POST['cutrx1'];
	$src_h    = $_POST['cutry1'];
	$bg_color = $_POST['bg_color'];
	$bg_pct   = $_POST['bg_pct'];
	if ( $type == "cutbg" ) {
		if ( $_SESSION['bgcha'] == 0 ) {
			$bg = $_SESSION['bg'];
		} elseif ( $_SESSION['bgcha'] == 1 ) {
			$bg = $_SESSION['changebg'];
		}
		$bg_size = getimagesize( $bg );
		switch ( $bg_size[2] ) {
			case 1:
				$src_im = imagecreatefromgif( $bg );
				break;
			case 2:
				$src_im = imagecreatefromjpeg( $bg );
				break;
			case 3:
				$src_im = imagecreatefrompng( $bg );
				break;
			default :
				$src_im = imagecreatefrompng( $bg );
		}
		$dst_im      = imagecreatetruecolor( $src_w, $src_h );
		$color_array = hex2rgb( $bg_color );
		$color       = imagecolorallocate( $dst_im, $color_array[0], $color_array[1], $color_array[2] );
		imagefilledrectangle( $dst_im, 0, 0, $src_w, $src_h, $color );
		imagecopymerge( $dst_im, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h, $bg_pct );

		mb_internal_encoding( "UTF-8" );
		header( "Content-Type:image/png" );
		imagepng( $dst_im, "upload/changecut/background.png" );
		$_SESSION['bgcut']    = 1;
		$_SESSION['changebg'] = "upload/changecut/background.png";
		imagedestroy( $dst_im );
		exit( '{"width":'.$src_w.',"height":'.$src_h.'}' );
	}
}

if ( isset( $_POST['type'] ) && isset( $_POST['charx1'] ) && isset( $_POST['chary1'] ) ) {
	$type     = $_POST['type'];
	$src_w    = $_POST['charx1'];
	$src_h    = $_POST['chary1'];
	if ( $type == "chabg" ) {
		if ( $_SESSION['bgcut'] == 0 ) {
			$bg = $_SESSION['bg'];
		} elseif ( $_SESSION['bgcut'] == 1 ) {
			$bg = $_SESSION['changebg'];
		}
		$bg_size = getimagesize( $bg );
		switch ( $bg_size[2] ) {
			case 1:
				$src_im = imagecreatefromgif( $bg );
				break;
			case 2:
				$src_im = imagecreatefromjpeg( $bg );
				break;
			case 3:
				$src_im = imagecreatefrompng( $bg );
				break;
			default :
				$src_im = imagecreatefrompng( $bg );
		}
		$dst_im      = imagecreatetruecolor( $src_w, $src_h );
		$white       = imagecolorallocate( $dst_im, 255, 255, 255 );
		imagefilledrectangle( $dst_im, 0, 0, $src_w, $src_h, $white );
		imagecopyresampled($dst_im, $src_im, 0, 0, 0, 0, $src_w, $src_h, $bg_size[0], $bg_size[1]);

		mb_internal_encoding( "UTF-8" );
		header( "Content-Type:image/png" );
		imagepng( $dst_im, "upload/changecha/background.png" );
		$_SESSION['bgcha']    = 1;
		$_SESSION['changebg'] = "upload/changecha/background.png";
		imagedestroy( $dst_im );
		exit( 'success' );
	}
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

if ( isset( $_POST['type'] ) && isset( $_POST['cutlx2'] ) && isset( $_POST['cutly2'] ) && isset( $_POST['cutrx2'] ) && isset( $_POST['cutry2'] ) && isset( $_POST['qr_color'] ) && isset( $_POST['qr_pct'] ) ) {
	$type     = $_POST['type'];
	$src_x    = $_POST['cutlx2'];
	$src_y    = $_POST['cutly2'];
	$src_w    = $_POST['cutrx2'];
	$src_h    = $_POST['cutry2'];
	$qr_color = $_POST['qr_color'];
	$qr_pct   = $_POST['qr_pct'];
	if ( $type == "cutqr" ) {
		if ( $_SESSION['qrcha'] == 0 ) {
			$qr = $_SESSION['qr'];
		} elseif ( $_SESSION['qrcha'] == 1 ) {
			$qr = $_SESSION['changeqr'];
		}
		$qr_size = getimagesize( $qr );
		switch ( $qr_size[2] ) {
			case 1:
				$src_im = imagecreatefromgif( $qr );
				break;
			case 2:
				$src_im = imagecreatefromjpeg( $qr );
				break;
			case 3:
				$src_im = imagecreatefrompng( $qr );
				break;
			default :
				$src_im = imagecreatefrompng( $qr );
		}
		$dst_im      = imagecreatetruecolor( $src_w, $src_h );
		$color_array = hex2rgb( $qr_color );
		$color       = imagecolorallocate( $dst_im, $color_array[0], $color_array[1], $color_array[2] );
		imagefilledrectangle( $dst_im, 0, 0, $src_w, $src_h, $color );
		imagecopymerge( $dst_im, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h, $qr_pct );

		mb_internal_encoding( "UTF-8" );
		header( "Content-Type:image/png" );
		imagepng( $dst_im, "upload/changecut/qrcode.png" );
		$_SESSION['qrcut']    = 1;
		$_SESSION['changeqr'] = "upload/changecut/qrcode.png";
		imagedestroy( $dst_im );
		exit( 'success' );
	}
}

if ( isset( $_POST['type'] ) && isset( $_POST['charx2'] ) && isset( $_POST['chary2'] ) ) {
	$type     = $_POST['type'];
	$src_w    = $_POST['charx2'];
	$src_h    = $_POST['chary2'];
	if ( $type == "chaqr" ) {
		if ( $_SESSION['qrcut'] == 0 ) {
			$qr = $_SESSION['qr'];
		} elseif ( $_SESSION['qrcut'] == 1 ) {
			$qr = $_SESSION['changeqr'];
		}
		$qr_size = getimagesize( $qr );
		switch ( $qr_size[2] ) {
			case 1:
				$src_im = imagecreatefromgif( $qr );
				break;
			case 2:
				$src_im = imagecreatefromjpeg( $qr );
				break;
			case 3:
				$src_im = imagecreatefrompng( $qr );
				break;
			default :
				$src_im = imagecreatefrompng( $qr );
		}
		$dst_im      = imagecreatetruecolor( $src_w, $src_h );
		$white       = imagecolorallocate( $dst_im, 255, 255, 255 );
		imagefilledrectangle( $dst_im, 0, 0, $src_w, $src_h, $white );
		imagecopyresampled($dst_im, $src_im, 0, 0, 0, 0, $src_w, $src_h, $qr_size[0], $qr_size[1]);

		mb_internal_encoding( "UTF-8" );
		header( "Content-Type:image/png" );
		imagepng( $dst_im, "upload/changecha/qrcode.png" );
		$_SESSION['qrcha']    = 1;
		$_SESSION['changeqr'] = "upload/changecha/qrcode.png";
		imagedestroy( $dst_im );
		exit( 'success' );
	}
}


function hex2rgb( $hex ) {
	$hex = str_replace( "#", "", $hex );

	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );
	}

	return array( $r, $g, $b );
}
