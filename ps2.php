<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/4
 * Time: 9:45
 */

session_start();

if ( isset( $_SESSION['bg'] ) && isset( $_SESSION['qr'] ) ) {
	if ( $_SESSION['bgcut'] == 1 || $_SESSION['bgcha'] == 1 ) {
		$bg = $_SESSION['changebg'];
	} elseif ($_SESSION['bgcut'] == 0 && $_SESSION['bgcha'] == 0) {
		$bg = $_SESSION['bg'];
	}
	if ( $_SESSION['qrcut'] == 1 || $_SESSION['qrcha'] == 1 ) {
		$qr = $_SESSION['changeqr'];
	} elseif ($_SESSION['qrcut'] == 0 && $_SESSION['qrcha'] == 0) {
		$qr = $_SESSION['qr'];
	}


	if ( isset( $_GET['mx'] ) && isset( $_GET['my'] ) && isset($_GET['pct'])) {
		psimgs( $bg, $qr, $_GET['mx'], $_GET['my'], 100, $_GET['pct'] );
	}
}
function psimgs( $bg, $qr, $move_x, $move_y, $pct_bg, $pct_qr ) {

	$bg_size = getimagesize( $bg );
	$qr_size = getimagesize( $qr );
	switch ( $bg_size[2] ) {
		case 1:
			$bg_img = imagecreatefromgif( $bg );
			break;
		case 2:
			$bg_img = imagecreatefromjpeg( $bg );
			break;
		case 3:
			$bg_img = imagecreatefrompng( $bg );
			break;
		default :
			$bg_img = imagecreatefrompng( $bg );
	}

	switch ( $qr_size[2] ) {
		case 1:
			$qr_img = imagecreatefromgif( $qr );
			break;
		case 2:
			$qr_img = imagecreatefromjpeg( $qr );
			break;
		case 3:
			$qr_img = imagecreatefrompng( $qr );
			break;
		default:
			$qr_img = imagecreatefrompng( $qr );
	}
	$bg_info   = getimagesize( $bg );
	$qr_info   = getimagesize( $qr );
	$dst_image = imagecreatetruecolor( $bg_info[0], $bg_info[1] );
	imagecopymerge( $dst_image, $bg_img, 0, 0, 0, 0, $bg_info[0], $bg_info[1], $pct_bg );
	imagecopymerge( $dst_image, $qr_img, $move_x, $move_y, 0, 0, $qr_info[0], $qr_info[1], $pct_qr );

	mb_internal_encoding( "UTF-8" );
	header( "Content-Type:image/png" );
	imagepng( $dst_image );
	imagepng( $dst_image, "out/out1.png" );
	imagedestroy( $dst_image );
}