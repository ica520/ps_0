<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/10
 * Time: 13:59
 */

require_once './phpqrcode/qrlib.php';

class makePoster
{

    function __construct()
    {

    }

    /**
     * 制作二维码功能
     * @param string $contentString 二维码内容
     * @param int $size 二维码纠错等级：0最低，1低，2中等，3高
     * @param int $newWidth 新的二维码宽度
     * @param int $newHeight 新的二维码高度
     * @param string $logo logo的图片链接
     * @return string 输出保存的制作完图片地址
     */
    protected function makeQRCode($contentString = '', $size = 0, $newWidth = 201, $newHeight = 201, $logo = '')
    {
        //创建随机数，用来给二维码命名
        $outName = $this->fileReName();
        $outName .= '.png';
        //设定输出二维码的相对路径
        $relative_path = "outs/$outName";
        //实例化一个QRCode类
        $phpQRCode = new QRcode();
        //制作出二维码
        $phpQRCode::png($contentString, $relative_path, $size);
        //二维码图片转资源
        $relative_Code = $this->createImageFromFile($relative_path);
        //将制作出的二维码图片资源宽度高度改成201，201，图片地址为刚制作出的二维码，输出图片资源
        $changeCode = $this->changeImagePixel($newWidth, $newHeight, $relative_Code);
        //如果要添加logo的
        if ($logo != '' && $logo != null) {
            //图片文件转成资源
            $logo_im = $this->createImageFromFile($logo);
            //将logo图片宽高改完
            $changeLogo = $this->changeImagePixel(45, 45, $logo_im);
            //将logo放到二维码上
            imagecopy($changeCode, $changeLogo, (201 / 2 - 45 / 2), (201 / 2 - 45 / 2), 0, 0, 45, 45);
        } else {
            $changeCode = $relative_Code;
        }
        imagepng($changeCode, $relative_path);
        imagedestroy($changeCode);
        //输出二维码保存地址
        return $relative_path;
    }

    /**
     * 随机数命名
     * @param int $size 复杂程度
     * @param int $length 长度
     * @return string
     */
    private function fileReName($size = 6, $length = 10)
    {
        //创建随机数，用来给二维码命名
        $numbers = range(0, 9);
        $letters_s = range('a', 'z');
        $letters_b = range('A', 'Z');
        switch ($size) {
            case 0:
                $changeStr = join('', $numbers);
                break;
            case 1:
                $changeStr = join('', $letters_s);
                break;
            case 2:
                $changeStr = join('', $letters_b);
                break;
            case 3:
                $changeStr = join("", array_merge($numbers, $letters_s));
                break;
            case 4:
                $changeStr = join("", array_merge($numbers, $letters_b));
                break;
            case 5:
                $changeStr = join("", array_merge($letters_s, $letters_b));
                break;
            case 6:
                $changeStr = join('', array_merge($numbers, $letters_s, $letters_b));
                break;
            default :
                $changeStr = join('', array_merge($numbers, $letters_s, $letters_b));
        }
        $outName = str_shuffle($changeStr){0};
        for ($i = 0; $i < $length; $i++) {
            $outName .= str_shuffle($changeStr){0};
        }
        return $outName;
    }

    /**
     * 设定图片宽高改变图片大小的缩放功能
     * @param int $width 改变宽度为
     * @param int $height 改变高度为
     * @param resource $image 图片资源
     * @return resource 改变后的图像资源
     */
    private function changeImagePixel($width = 0, $height = 0, $image)
    {
        //创图片背景，大小为要设定的大小
        $image_bg = imagecreatetruecolor($width, $height);
        //透明色
        $transparent = imagecolorallocatealpha($image_bg, 0, 0, 0, 127);
        //上透明色
        imagefill($image_bg, 0, 0, $transparent);
        //获取图片宽高
        list($image_width, $image_height) = array(imagesx($image), imagesy($image));
        //缩放
        imagecopyresampled($image_bg, $image, 0, 0, 0, 0, $width, $height, $image_width, $image_height);
        //输出图像资源
        return $image_bg;
    }

    /**
     * 旋转照片：
     * @param resource $image
     * @param int $rotate
     * @return resource
     */
    private function rotateImage($image, $rotate = 0)
    {
        $transparent = imagecolorallocatealpha($image, 0, 0, 0, 127);
        $rotated = imagerotate($image, $rotate, $transparent, 1);
        return $rotated;
    }

    /**
     * 裁剪图片功能
     * @param resource $image 需要裁剪的图片
     * @param array $new_image_point 裁剪左上角坐标
     * @param int $new_image_w 裁剪后宽度
     * @param int $new_image_h 裁剪后高度
     * @return resource 裁剪后的图像资源
     */
    protected function cutImage($image, $new_image_point = array(0, 0), $new_image_w = 0, $new_image_h = 0)
    {
        list($new_image_x, $new_image_y) = $new_image_point;
        //创建背景为裁剪后大小
        $image_bg = imagecreatetruecolor($new_image_w, $new_image_h);
        //透明色
        $transparent = imagecolorallocatealpha($image_bg, 0, 0, 0, 127);
        //填充透明色
        imagefill($image_bg, 0, 0, $transparent);
        //裁剪
        imagecopyresampled($image_bg, $image, 0, 0, $new_image_x, $new_image_y, $new_image_w, $new_image_h, $new_image_w, $new_image_h);
        return $image_bg;
    }

    /**
     * 从文件转成图像资源功能，输出图像资源
     * @param string $image_file
     * @return resource
     */
    private function createImageFromFile($image_file = '')
    {
        list($image_width, $image_height, $image_size) = getimagesize($image_file);
        $image_bg = imagecreatetruecolor($image_width, $image_height);
        $transparent = imagecolorallocatealpha($image_bg, 0, 0, 0, 127);
        imagefill($image_bg, 0, 0, $transparent);
        //图片转成图像资源
        switch ($image_size) {
            case 1:
                $image = imagecreatefromgif($image_file);
                imagecopyresampled($image_bg, $image, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height);
                return $image_bg;
                break;
            case 2:
                $image = imagecreatefromjpeg($image_file);
                imagecopyresampled($image_bg, $image, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height);
                return $image_bg;
                break;
            case 3:
                $image = imagecreatefrompng($image_file);
                imagecopyresampled($image_bg, $image, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height);
                return $image_bg;
                break;
            default:
                // TODO ...
                //如果图片有问题则输出这个图片
                $image_bg_error = imagecreatetruecolor($image_width, $image_height);
                //白色背景
                $white = imagecolorallocate($image_bg_error, 255, 255, 255);
                //填充
                imagefill($image_bg_error, 0, 0, $white);
                //灰色字体
                $gray = imagecolorallocate($image_bg_error, 150, 150, 150);
                imagettftext($image_bg_error, 5, 0, 0, 0, $gray, __DIR__ . '/fonts/Deng.ttf', '错误图片！');
                return $image_bg_error;
        }
    }

    //图像，起点，字体，文字，大小，一行多少字，行距, 是否居中
    protected function writeText($image, $start_point = array(115, 585), $font = '', $text = '内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，', $size = 19, $line_count = 35, $line_height = 0, $center = true)
    {
        $black = imagecolorallocate($image, 0, 0, 0);
        $start_x = $start_point[0];
        $start_y = $start_point[1];
        $content_array = $this->mb_str_split($text);
        $count_content = count($content_array);

        for ($j = 0; $j < ($count_content / $line_count); $j++) {
            if ($count_content - $j * $line_count > $line_count) {
                for ($i = 0; $i < $line_count; $i++) {
                    $text_position_x = $start_x + ($size + 5) * $i;
                    $text_position_y = $start_y + $line_count * $j + $line_height;
                    imagettftext($image, $size, 0, $text_position_x, $text_position_y, $black, $font, $content_array[($j * $line_count + $i)]);
                }
            } else {
                for ($i = 0; $i < ($count_content - $j * $line_count); $i++) {
                    $msi = $count_content % $line_count;
                    $text_position_x = $start_x + ($size + 5) * $i;
                    if ($center){
                        $text_position_x = $text_position_x + ($line_count / 2 * ($size + 5) - $msi / 2 * ($size + 5));
                    }
                    $text_position_y = $start_y + $line_count * $j + $line_height;
                    imagettftext($image, $size, 0, $text_position_x, $text_position_y, $black, $font, $content_array[($j * $line_count + $i)]);
                }
            }
        }
        return $image;
    }

    /**
     * 将字符串分割为数组
     *
     * @param  string $str 字符串
     *
     * @return array       分割得到的数组
     */
    public function mb_str_split($str)
    {
        return preg_split('/(?<!^)(?!$)/u', $str);
    }

    /**
     * 制作二维码
     * @param string $content
     * @param int $size
     * @param string $logo
     * @return string
     */
    public function QRCode($content = '', $size = 0, $logo = '')
    {
        return $this->makeQRCode($content, $size, 201, 201, $logo);
    }

    /**
     * 改变图片大小
     * @param int $width
     * @param int $height
     * @param resource $image
     * @return resource
     */
    public function imagePixel($width = 0, $height = 0, $image)
    {
        return $this->changeImagePixel($width, $height, $image);
    }

    /**
     * 裁剪图片
     * @param resource $image
     * @param array $im_point
     * @param int $im_w
     * @param int $im_h
     * @return resource
     */
    public function imageCut($image, $im_point, $im_w, $im_h)
    {
        return $this->cutImage($image, $im_point, $im_w, $im_h);
    }

    /**
     * 从文件创建图像资源
     * @param string $file
     * @return resource
     */
    public function imageCreateFromFile($file = '')
    {
        return $this->createImageFromFile($file);
    }

    /**
     * @param int $size
     * @param int $length
     * @return string
     */
    public function reName($size = 6, $length = 10)
    {
        return $this->fileReName($size, $length);
    }

    /**
     * 旋转照片
     * @param resource $image
     * @param int $rotate
     * @return resource
     */
    public function imageRotate($image, $rotate = 0)
    {
        return $this->rotateImage($image, $rotate);
    }

    function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}