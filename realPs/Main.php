<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>制作海报</title>
</head>
<body>

<span style="font-size: 25px; color: #FF0000;">没有二维码内容：</span><br/>
是否需要中心logo：<input id="nonQRCode-needLogo" type="checkbox" checked="checked"/><br/>
<form id="post-box" method="post" action="makeQRCode.php" enctype="multipart/form-data">
    生成二维码：<br/>
    <input name="upload-nonQRCode-content" type="text"/><br/>
    <div id="nonQRCode-logo"><input name="upload-nonQRCode-logo" type="file"/><br/></div>
    <input id="submit" type="button" value="提交"/><br/>
</form>

<span style="font-size: 25px; color: #FF0000;">有二维码：</span><br/>
二维码：<br/>
<input id="upload-QRCode" type="file"/><br/>
需要裁剪：<input id="cutQRCode" type="checkbox"/><br/>
中心logo：<br/>
<input id="upload-QRCodeLogo" type="file"/><br/>
<input type="button" value="提交"/><br/>

<span style="font-size: 25px; color: #FF0000;">不需要二维码：</span><br/>
<input type="button" value="提交"/><br/>
<div id="outImage"></div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $("#submit").click(function () {
            $("#post-box").ajaxSubmit({
                dataType: "json", //数据格式为json
                success: function (data) {
                    alert("上传成功");
                    var src = data.url;
                    $("#outImage").html("<img src='" + src + "?r=" + Math.random() + "'>");
                },
                error: function (xhr, tS, rT) { //上传失败
                    alert("上传失败\n" + xhr + "\n" + tS + "\n" + rT);
                }
            });
        });
        $("#nonQRCode-needLogo").click(function () {
            if ($("#nonQRCode-needLogo").is(":checked")) {
                $("#nonQRCode-logo").html("<input name=\"upload-nonQRCode-logo\" type=\"file\"/><br/>");
            } else {
                $("#nonQRCode-logo").html("");
            }
        });
    });
</script>
</body>
</html>

<?php
?>