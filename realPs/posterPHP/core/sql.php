<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/13
 * Time: 9:41
 */

class SqlS
{
    protected $conn;
    protected $res;

    function __construct()
    {
        // 1.链接数据库
        $conn = @new mysqli('localhost', 'root', '', 'poster_system');
        if ($conn->connect_error) {//返回链接错误号
            // 返回链接错误信息
            die('数据库链接错误 (' . $conn->connect_errno . ') '
                . $conn->connect_error);
        }
        // 2. 选择数据库
        $conn->select_db("poster_system") or die("选择数据库失败：" . $conn->error);
        $conn->set_charset('utf8');
        $conn->query("set character set 'utf8'");
        $conn->query("set names 'utf8'");
        $this->conn = $conn;
    }

    public function showQuestions()
    {
        // 默认问题：
        $sql = 'SELECT pk_questions_id, qs_Name, qs_Type FROM tbl_questions WHERE  qs_isShow = 1 AND qs_CreateUid = 1 ORDER BY pk_questions_id ASC';
        //选择的记录
        if (isset($_SESSION['temp'])) {
            $selected_array = explode('|', $_SESSION['temp']);
        } else {
            $selected_array = array();
        }
        // var_dump($selected_array);
        $res = $this->conn->query($sql);
        $num = $res->num_rows;
        for ($i = 0; $i < $num; $i++) {
            $row = $res->fetch_row();
            if ($i == 0 || in_array($row[1], $selected_array)) {
                if ($row[2] == 2) {
                    ?>
                    <a href="javascript:;" onclick="ajax_selectCourse(<?= $row[0] ?>)">
                        <li class="active"><?= $row[1] ?></li>
                    </a>
                    <?php
                } else {
                    ?>
                    <li class="active"><?= $row[1] ?></li>
                    <?php
                }
            } else {
                if ($row[2] == 2) {
                    ?>
                    <a href="javascript:;" onclick="ajax_selectCourse(<?= $row[0] ?>)">
                        <li><?= $row[1] ?></li>
                    </a>
                    <?php
                } else {
                    ?>
                    <li><?= $row[1] ?></li>
                    <?php
                }
            }
        }
        // 用户自定义的问题：
        if ($_SESSION['uid'] != 1) {
            $uid = $_SESSION['uid'];
            $sql = "SELECT pk_questions_id, qs_Name, qs_isShow FROM tbl_questions WHERE  qs_CreateUid = $uid ORDER BY pk_questions_id ASC";
            $res = $this->conn->query($sql);
            $num = $res->num_rows;
            for ($i = 0; $i < $num; $i++) {
                $row = $res->fetch_row();
                if ($row[2] == 1) {
                    ?>
                    <a href="javascript:;" onclick="ajax_selectDiyCourse(<?= $row[0] ?>)">
                        <li class="active"><?= $row[1] ?></li>
                    </a>
                    <?php
                } else {
                    ?>
                    <a href="javascript:;" onclick="ajax_selectDiyCourse(<?= $row[0] ?>)">
                        <li><?= $row[1] ?></li>
                    </a>
                    <?php
                }
            }
        }
    }

    public function courseWrite($id, $temp, $uid)
    {
        //查出选择的时什么内容：
        $sql = "SELECT qs_Modes, qs_DefaultText, qs_Name FROM tbl_questions WHERE pk_questions_id = $id";
        $res = $this->conn->query($sql);
        $row = $res->fetch_row();
        if (isset($temp)) {
            $selected_array = explode('|', $temp);
        } else {
            $selected_array = array();
        }
        if (!in_array($row[2], $selected_array))
            $_SESSION['temp'] .= $row[2] . '|';
        $much = explode('|', $row[0]);
        //查出已经写过的数据
        $sql_content = "SELECT qc_QuContent FROM tbl_question_content WHERE qc_userId = $uid ORDER BY pk_question_content_id ASC";
        $res_content = $this->conn->query($sql_content);
//        $num_content = $res_content->num_rows;
        for ($i = 0; $i < $much[0]; $i++) {
//            if ($i < $num_content) {
            $row_content = $res_content->fetch_row();
            $content = $row_content[0];
//            } else
//                $content = '';
            $num = $i + 1;
            ?>
            <li>
                <span><?= $num ?></span>
                <input type="text" placeholder="<?= $row[1] ?>" value="<?= $content ?>"/>
            </li>
            <?php
        }
    }

    public function addCourses($string = '', $id, $uid)
    {
        $string_array = explode(';', $string);
        $length = count($string_array);
        // 删除所有空白
        for ($i = 0; $i < $length; $i++) {
            $key = array_search('', $string_array);
            if ($key !== false)
                array_splice($string_array, $key, 1);
        }
        // 删除最后一个空白
//        array_pop($string_array);
        $sql = "DELETE FROM tbl_question_content WHERE qc_userId = $uid AND qc_QuId = $id";
        $this->conn->query($sql);
        $num = count($string_array);
        for ($i = 0; $i < $num; $i++) {
            $content = $string_array[$i];
//            echo $content;
            $content = addslashes(trim($content));
            $sql = "INSERT INTO tbl_question_content (qc_userId, qc_QuId, qc_QuContent)VALUES($uid, $id, '$content')";
            $this->conn->query($sql);
        }
    }

    public function seSelectDiyQuCo($qId)
    {
        $sql = "SELECT qs_Name, qs_isShow, qs_Type, qs_Modes FROM tbl_questions WHERE pk_questions_id = $qId";
        $res = $this->conn->query($sql);
        return $res->fetch_row();
    }

    public function inputDiyQuestion($qId, $qName, $qShow, $qType, $qTexts, $uid)
    {
        $qName = addslashes(trim($qName));
        $qText_array = explode('|', $qTexts);
        $qText_array_count = count($qText_array);
        for ($i = 0; $i < $qText_array_count; $i++) {
            $key = array_search('', $qText_array);
            if ($key !== false)
                array_splice($qText_array, $key, 1);
        }
        $qText_array_count = count($qText_array);
        $qModes = '|';
        for ($i = 0; $i < $qText_array_count; $i++) {
            $qModes .= addslashes(trim($qText_array[$i])) . ';';
        }
        if ($qId == 'false') {
            // 如果没有问题id那么是新建问题
            $sql = "INSERT INTO tbl_questions(`qs_CreateUid`, `qs_Name`, `qs_isShow`, `qs_Type`, `qs_Modes`)VALUES($uid, '$qName', $qShow, '$qType', '$qModes')";
        } else {
            $sql = "UPDATE tbl_questions SET qs_Name = '$qName', qs_isShow = $qShow, qs_Type = '$qType', qs_Modes = '$qModes' WHERE pk_questions_id = $qId";
        }
        $res = $this->conn->query($sql);
        if ($res)
            return true;
        else
            return false;
    }

    /**
     * 作成表格：
     * @param string $quName
     * @return mixed
     */
    public function buildDeQuestion($quName)
    {
        $quName = addslashes(trim($quName));
        $sql = "SELECT pk_questions_id, qs_Type, qs_Modes FROM tbl_questions WHERE qs_Name = '$quName' AND qs_CreateUid = 1";
        $res = $this->conn->query($sql);
        return $res->fetch_row();
    }

    public function searchQuContent($uid, $qid)
    {
        $sql = "SELECT qc_QuContent FROM tbl_question_content WHERE qc_userId = $uid AND qc_QuId = $qid ORDER BY pk_question_content_id ASC";
        $res = $this->conn->query($sql);
        $num_rows = $res->num_rows;
        $result_array = array();
        for ($i = 0; $i < $num_rows; $i++) {
            $rows = $res->fetch_row();
            $result_array[] = $rows[0];
        }
        return $result_array;
    }

    public function searchDiShowQuContent($uid)
    {
        $sql = "SELECT qs_Name, qs_Type, qs_Modes FROM tbl_questions WHERE qs_CreateUid = $uid AND qs_isShow ORDER BY pk_questions_id ASC";
        $res = $this->conn->query($sql);
        return $res;
    }

    // 做成表格
    public function makeQrCodeNow($uid, $temp, $fid)
    {
        // 是修改表还是创建表：
        $sql_hasForm = "SELECT 1 FROM tbl_tables WHERE ts_UserId = $uid AND ts_TaId = $fid";
        $res = $this->conn->query($sql_hasForm);
        if ($res->num_rows == 0)
            $hadForm = false;
        else
            $hadForm = true;
        // 如果已经有了，删除所有，重新写入
        if ($hadForm) {
            $sql_del_form = "DELETE FROM tbl_tables WHERE ts_UserId = $uid AND ts_TaId = $fid";
            $this->conn->query($sql_del_form);
        }
        // 插入默认问题
        list($count, $temp_array) = $this->explodeTemp($temp);
        for ($i = 0; $i < $count; $i++) {
            // 查出所有选中的默认问题
            $qName = $temp_array[$i];
            $sql = "SELECT pk_questions_id, qs_Type, qs_Modes, qs_DefaultText FROM tbl_questions WHERE qs_CreateUid = 1 AND qs_isShow = 1 AND qs_Name = '$qName'";
            $res = $this->conn->query($sql);
            if (($row = $res->fetch_row()) != false) {
                list($qId, $qType, $qModes, $qBaText) = $row;
                if ($qType == 2) {
                    $sql_choose_things = "SELECT qc_QuContent FROM tbl_question_content WHERE qc_userId = $uid AND qc_QuId = $qId";
                    $res_choose = $this->conn->query($sql_choose_things);
                    $num_rows = $res_choose->num_rows;
                    $add_rows = '';
                    for ($j = 0; $j < $num_rows; $j++) {
                        $rows_choose = $res_choose->fetch_row();
                        $add_rows .= $rows_choose[0] . ';';
                    }
                    $d_qModes = $num_rows . '|' . $add_rows;
                    $sql_insert = "INSERT INTO tbl_tables(ts_UserId, ts_TaId, ts_QuName, ts_QuType, ts_QuModes, ts_QuBaText)VALUES($uid, $fid, '$qName', $qType, '$d_qModes', '$qBaText')";
                    $this->conn->query($sql_insert);
                } else {
                    $sql_insert = "INSERT INTO tbl_tables(ts_UserId, ts_TaId, ts_QuName, ts_QuType, ts_QuModes, ts_QuBaText)VALUES($uid, $fid, '$qName', $qType, '$qModes', '$qBaText')";
                    $this->conn->query($sql_insert);
                }
            }
        }
        // 插入自定义问题：
        // 查询自定义启用的问题：
        $sql_diy_show = "SELECT qs_Name, qs_Type, qs_Modes, qs_DefaultText FROM tbl_questions WHERE qs_isShow = 1 AND qs_CreateUid = $uid";
        $res = $this->conn->query($sql_diy_show);
        $row_num = $res->num_rows;
        for ($i = 0; $i < $row_num; $i++) {
            $rows = $res->fetch_row();
            $sql_insert_diy = "INSERT INTO tbl_tables(ts_UserId, ts_TaId, ts_QuName, ts_QuType, ts_QuModes, ts_QuBaText)VALUES($uid, $fid, '$rows[0]', $rows[1], '$rows[2]', '$rows[3]')";
            $this->conn->query($sql_insert_diy);
        }
    }

    /**
     * 查询默认图片
     * @param int $type 图片类型 目前 1 背景， 2 标题
     * @return array|bool
     */
    public function listDeImage($type)
    {
        $sql = "SELECT di_imUrl_a, di_imUrl_b FROM tbl_default_images WHERE di_imType = $type ORDER BY pk_default_images_id ASC";
        $res = $this->conn->query($sql);
        $num_rows = $res->num_rows;
        if ($num_rows > 0) {
            $list_a = array();
            $list_b = array();
            for ($i = 0; $i < $num_rows; $i++) {
                $row_array = $res->fetch_row();
                $list_a[] = $row_array[0];
                $list_b[] = $row_array[1];
            }
            return array($list_a, $list_b);
        } else {
            return false;
        }
    }

    /**
     * 把默认图片链接转为id
     * @param string $url
     * @param int $type ：1 背景 2 标题
     * @return int
     */
    public function changeDeImageTextToId($url, $type)
    {
        $sql = "SELECT di_imUrl_a FROM tbl_default_images WHERE di_imType = $type ORDER BY pk_default_images_id ASC";
        $res = $this->conn->query($sql);
        $num_rows = $res->num_rows;
        for ($i = 0; $i < $num_rows; $i++) {
            $row_array = $res->fetch_row();
            if ($row_array[0] == $url) {
                return $i;
            }
        }
    }

    // 把前端图片转后端图片：
    public function changeAImToBIm($im_a, $type)
    {
        $im_a = addslashes(trim($im_a));
        $type = addslashes(trim($type));
        $sql = "SELECT di_imUrl_b, di_diy_x, di_diy_y FROM tbl_default_images WHERE di_imUrl_a = '$im_a' AND di_imType = '$type'";
        $res = $this->conn->query($sql);
        $row = $res->fetch_row();
        return $row;
    }

    /**
     * 查询已经做好的表格信息
     * @param int $uid
     * @param int $fid
     * @return array
     */
    public function searchFormInformation($uid, $fid)
    {
        $uid = addslashes(trim($uid));
        $fid = addslashes(trim($fid));
        $sql = "SELECT ts_QuName, ts_QuType, ts_QuModes, ts_QuBaText FROM tbl_tables WHERE ts_UserId = '$uid' AND ts_TaId = '$fid' ORDER BY pk_tables_id";
        $res = $this->conn->query($sql);
        $num_rows = $res->num_rows;
        $row = array();
        for ($i = 0; $i < $num_rows; $i++) {
            $row[] = $res->fetch_row();
        }
        return array($num_rows, $row);
    }

    // 填表：
    public function addForm($array, $cid, $fid)
    {
        $uid = $this->fileReName(0, 5);
        $text_array = $array[0];
        $radio_array = $array[1];
        $box_array = $array[2];
        $m = 0;
        for ($i = 0; $i < count($text_array); $i++) {
            $m++;
            $text_name = $text_array[$i][0];
            $text_answer = $text_array[$i][1];
            $text_answer = $text_answer . ';';
            $sql = "INSERT INTO tbl_table_content(tc_UserId, tc_CId, tc_TaId, tc_QuId, tc_QuType, tc_QuName, tc_QuAnswer)VALUES($uid, $cid, $fid, $m, 1, '$text_name', '$text_answer')";
            $this->conn->query($sql);
        }
        for ($i = 0; $i < count($radio_array); $i++) {
            $m++;
            $text_name = $radio_array[$i][0];
            $text_answer = $radio_array[$i][1];
            $text_answer = $text_answer . ';';
            $sql = "INSERT INTO tbl_table_content(tc_UserId, tc_CId, tc_TaId, tc_QuId, tc_QuType, tc_QuName, tc_QuAnswer)VALUES($uid, $cid, $fid, $m, 2, '$text_name', '$text_answer')";
            $this->conn->query($sql);
        }
        for ($i = 0; $i < count($box_array); $i++) {
            $m++;
            $text_name = $box_array[$i][0];
            $text_answer_array = $box_array[$i][1];
            $text_answer = '';
            for ($j = 0; $j < count($text_answer_array); $j++) {
                $text_answer .= $text_answer_array[$j] . ';';
            }
            $sql = "INSERT INTO tbl_table_content(tc_UserId, tc_CId, tc_TaId, tc_QuId, tc_QuType, tc_QuName, tc_QuAnswer)VALUES($uid, $cid, $fid, $m, 3, '$text_name', '$text_answer')";
            $this->conn->query($sql);
        }
    }

    // 列出表格
    public function listForm($uid)
    {
        $sql = "SELECT DISTINCT tc_UserId FROM tbl_table_content WHERE tc_CId = $uid";
        $res = $this->conn->query($sql);
        $num = $res->num_rows;
        $array = array();
        for ($i = 0; $i < $num; $i++) {
            $row = $res->fetch_row();
            $array[] = $row[0];
        }
        return $array;
    }

    // 列出问题及其答案
    public function listAnswer($uSid)
    {
        $sql = "SELECT DISTINCT tc_QuName FROM tbl_table_content WHERE tc_UserId = $uSid";
        $res = $this->conn->query($sql);
        $num = $res->num_rows;
        $array = array();
        for ($i = 0; $i < $num; $i++) {
            $row = $res->fetch_row();
            $sql2 = "SELECT tc_QuAnswer FROM tbl_table_content WHERE tc_QuName = '$row[0]' AND tc_UserId = $uSid ORDER BY pk_table_content_id ASC";
            $res2 = $this->conn->query($sql2);

            $row2 = $res2->fetch_row();
            $ans = $row2[0];

            $array[] = array($row[0], $ans);
        }
        return $array;
    }

    // 添加图片
    public function addTimeOutImages($uid, $file)
    {
        $time = time();
        $outTime = $time + 1209600;// (14*24*3600)
        $sql = "INSERT INTO tbl_image_recycle_bin(qrb_userId, qrb_imageUrl, qrb_createTime, qrb_deleteTime)VALUES($uid, '$file', $time, $outTime)";
        $this->conn->query($sql);
        $this->delTimeOutImages();
    }

    // 清理过期图片
    public function delTimeOutImages()
    {
        $time = time();
        $sql = "SELECT qrb_imageUrl FROM tbl_image_recycle_bin WHERE qrb_deleteTime < $time";
        $res = $this->conn->query($sql);
        if (!$res) {
            echo $this->conn->error;
        }
        $num = $res->num_rows;
        for ($i = 0; $i < $num; $i++) {
            $row = $res->fetch_row();
            unlink($row[0]);
        }
        $sql = "DELETE FROM tbl_image_recycle_bin WHERE qrb_deleteTime < $time";
        $re = $this->conn->query($sql);
        if (!$re) {
            echo $this->conn->error;
        }
    }

    /**
     * 分割 session temp e.g. "123|123|123|159|"
     * @param string $temp
     * @return array
     */
    public function explodeTemp($temp)
    {
        $temp_array = explode('|', $temp);
        $length = count($temp_array);
        // 删除所有空白
        for ($i = 0; $i < $length; $i++) {
            $key = array_search('', $temp_array);
            if ($key !== false)
                array_splice($temp_array, $key, 1);
        }
        $length = count($temp_array);
        return array($length, $temp_array);
    }

    /**
     * 随机数命名
     * @param int $size 复杂程度
     * @param int $length 长度
     * @return string
     */
    public function fileReName($size = 6, $length = 10)
    {
        //创建随机数，用来给二维码命名
        $numbers = range(0, 9);
        $letters_s = range('a', 'z');
        $letters_b = range('A', 'Z');
        switch ($size) {
            case 0:
                $changeStr = join('', $numbers);
                break;
            case 1:
                $changeStr = join('', $letters_s);
                break;
            case 2:
                $changeStr = join('', $letters_b);
                break;
            case 3:
                $changeStr = join("", array_merge($numbers, $letters_s));
                break;
            case 4:
                $changeStr = join("", array_merge($numbers, $letters_b));
                break;
            case 5:
                $changeStr = join("", array_merge($letters_s, $letters_b));
                break;
            case 6:
                $changeStr = join('', array_merge($numbers, $letters_s, $letters_b));
                break;
            default :
                $changeStr = join('', array_merge($numbers, $letters_s, $letters_b));
        }
        $outName = str_shuffle($changeStr){0};
        for ($i = 0; $i < $length; $i++) {
            $outName .= str_shuffle($changeStr){0};
        }
        return $outName;
    }

    function __destruct()
    {
        // TODO: Implement __destruct() method.
        // 10.释放结果集资源
        if (isset($this->res) && !is_bool($this->res))
            $this->res->close();// $res->free();
        // 关闭数据库连接
        $this->conn->close();
    }
}