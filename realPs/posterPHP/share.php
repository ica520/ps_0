<?php
require_once './header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
    <title>报名海报制作</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
<?php
if (isset($_GET['uid']) && isset($_SESSION['fid'])) {
    $g_uid = $_GET['uid'];
    $g_fid = $_GET['fid'];
    $image_url = './posters/' . $g_uid . '-' . $g_fid . '.jpg';
} else {
    exit('非法访问');
}
if (isset($_SESSION['uid'])){
if ($_SESSION['uid'] == $g_uid){
?>
<div class="share-cover cover fs36">
    <?php
    }else{
    ?>
    <div class="share-cover cover fs36" style="display: none;">
        <?php
        }
        }else{
        ?>
        <div class="share-cover cover fs36" style="display: none;">
            <?php
            }
            ?>
            <div class="a">知道了</div>
            <img class="b" src="./img/ball.png" alt="ball">
            <img class="d" src="./img/link.png" alt="link">
            <p class="c">点击右上角分享链接</p>
        </div>
        <div class="share">
            <img src="<?= $image_url ?>" alt="">
        </div>
        <script src='./script/jquery.js'></script>
        <script src='./script/index.js'></script>
        <script>
            $(".a").click(function () {
                $(".share-cover.cover.fs36").css("display", "none")
            })
        </script>
</body>
</html>