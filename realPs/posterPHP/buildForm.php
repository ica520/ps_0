<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
        <title>报名表 (预览)</title>
        <link rel="stylesheet" href="./css/base.css">
        <link rel="stylesheet" href="./css/index.css">
    </head>
    <body>
    <div class="bulid-form fs36">
        <ul class="lists flex">
            <?php
            // 默认问题
            if (isset($_SESSION['temp'])) {
                $temp = $_SESSION['temp'];
                $temp_array = explode('|', $temp);
                $temp_length = count($temp_array);
                // 删除所有空白
                for ($i = 0; $i < $temp_length; $i++) {
                    $key = array_search('', $temp_array);
                    if ($key !== false)
                        array_splice($temp_array, $key, 1);
                }
                $temp_length = count($temp_array);
                for ($i = 0; $i < $temp_length; $i++) {
                    list($qid, $type, $modes) = $sql->buildDeQuestion($temp_array[$i]);
                    switch ($type) {
                        case '1':
                            // 单选
                            $modes_array = explode('|', $modes);
                            $modes_length = $modes_array[0];
                            $modes_selects_array = explode(';', $modes_array[1]);
                            $modes_selects_count = count($modes_selects_array);
                            // 删除所有空白
                            for ($j = 0; $j < $modes_selects_count; $j++) {
                                $key = array_search('', $modes_selects_array);
                                if ($key !== false)
                                    array_splice($modes_selects_array, $key, 1);
                            }
                            $modes_selects_count = count($modes_selects_array);
                            ?>
                            <li>
                                <span><?= $temp_array[$i] ?></span>
                                <div class="radio">
                                    <?php
                                    for ($m = 0; $m < $modes_selects_count; $m++) {
                                        if ($m == 0) {
                                            ?>
                                            <span class="active">
                                        <?= $modes_selects_array[$m] ?>
                                        </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span>
                                        <?= $modes_selects_array[$m] ?>
                                        </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                            break;
                        case '2':
                            // 多选
                            $content_array = $sql->searchQuContent($_SESSION['uid'], $qid);
                            $content_count = count($content_array);
                            ?>
                            <li class="checkbox">
                                <span><?= $temp_array[$i] ?>选择</span><span>(可多选)</span>
                                <ul class="flex fs36">
                                    <?php
                                    for ($n = 0; $n < $content_count; $n++) {
                                        if ($n == 0) {
                                            ?>
                                            <li class="active"><?= $content_array[$n] ?></li>
                                            <?php
                                        } else {
                                            ?>
                                            <li><?= $content_array[$n] ?></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            break;
                        case '3' || '6':
                            // 单行文本
                            ?>
                            <li>
                                <span><?= $temp_array[$i] ?></span>
                                <input type="text">
                            </li>
                            <?php
                            break;
                        default :
                    }
                }
            }
            //自定义问题：
            if (isset($_SESSION['uid'])) {
                $res = $sql->searchDiShowQuContent($_SESSION['uid']);
                $res_count = $res->num_rows;
                for ($m = 0; $m < $res_count; $m++) {
                    list($dName, $dType, $dModes) = $res->fetch_row();
                    $dModes_array_temp = explode('|', $dModes);
                    $dModes_string = $dModes_array_temp[1];
                    $dModes_array = explode(';', $dModes_string);
                    $dModes_count = count($dModes_array);
                    // 删除所有空白
                    for ($i = 0; $i < $dModes_count; $i++) {
                        $key = array_search('', $dModes_array);
                        if ($key !== false)
                            array_splice($dModes_array, $key, 1);
                    }
                    $count_dModes = count($dModes_array);
                    switch ($dType) {
                        case '1':
                            // 单选
                            ?>
                            <li>
                                <span><?= $dName ?></span>
                                <div class="radio">
                                    <?php
                                    for ($j = 0; $j < $count_dModes; $j++) {
                                        if ($j == 0) {
                                            ?>
                                            <span class="active">
                                        <?= $dModes_array[$j] ?>
                                        </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span>
                                        <?= $dModes_array[$j] ?>
                                        </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                            break;
                        case '2':
                            // 多选
                            ?>
                            <li class="checkbox">
                                <span><?= $dName ?>选择</span><span>(可多选)</span>
                                <ul class="flex fs36">
                                    <?php
                                    for ($n = 0; $n < $count_dModes; $n++) {
                                        if ($n == 0) {
                                            ?>
                                            <li class="active"><?= $dModes_array[$n] ?></li>
                                            <?php
                                        } else {
                                            ?>
                                            <li><?= $dModes_array[$n] ?></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            break;
                        default :
                    }
                }
            }
            ?>
        </ul>
        <div class="btn">
            <button>
                <a href="./makeForm.php">返回修改</a>
            </button><button onclick="ajax_saveForm()">
                <a class="wait" href="javascript:void(0);">保存并继续制作海报 ></a>
            </button>
        </div>
    </div>
    </body>
    </html>
<?php
require_once './footer.php';