<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
        <title>自定义报名表</title>
        <link rel="stylesheet" href="./css/base.css">
        <link rel="stylesheet" href="./css/index.css">
    </head>
    <body>
    <div class="make-form">
        <p>请选择内容</p>
        <ul class="lists flexlists flex">
            <?php
            $sql->showQuestions();
            ?>
            <li><a href="#" onclick="ajax_addDiyQuestion()">+自定义问题</a></li>
        </ul>
        <div class="btn">
            <button onclick="ajax_selectCourse()"><a href="#">预览报名表</a></button>
        </div>
    </div>
    </body>
    </html>
<?php
require_once './footer.php';
?>
