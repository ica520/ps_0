//

function ajaxSubmieImage(image, url, box = "0") {
    $(image).ajaxSubmit({
        dataType: "json", //数据格式为json
        success: function (data) {
            if (data.status == 0) {
                if (box == "box") {
                    var src = data.url;
                    var width = data.width;
                    var height = data.height;
                    var strUrl = src + "?r=" + Math.random();
                    // 盒子里的图片
                    $(".cropper-view-box").find("img").attr("src", strUrl);
                    // 最底层的图片
                    $(".cropper-canvas").find("img").attr("src", strUrl);
                    //
                    $(".box1").css("z-index", 9999999999999)
                    console.log(3321)
                } else {
                    var src = data.url;
                    var strUrl = src + "?r=" + Math.random();
                    $(url).attr("src", strUrl);
                }
            } else if (data.status == 10) {
                var src = data.url;
                var strUrl = '<img src="' + src + '" alt="">';
                $(url).html(strUrl);
            } else {
                alert(data.content);
            }
        },
        error: function (xhr, tS, rT) { //上传失败
            alert("上传失败");
            return 0;
        }
    });
}

function ajax_selectCourse(selectPid = '') {
    var list = $(".active");
    var listText = "";
    for (var i = 0; i < list.length; i++) {
        var net = list[i].innerHTML + "|";
        listText += net;
    }
    $.ajax({
            url: "posts.php",
            type: "POST",
            async: true,
            timeOut: 50000,
            dataType: "json",
            data: {
                type: "addCoursesInTemp",
                listText: listText
            },
            success: function (data, textStatus, jqXHR) {
                if (data.content == 0) {
                    if (selectPid != '')
                        window.location.href = "./course.php?id=" + selectPid;
                    else
                        window.location.href = "./buildForm.php";
                } else
                    alert(data.error);
            }
            ,
            error: function (xhr, textStatus) {
                alert(xhr + textStatus);
            }
        }
    )
    ;
}

function ajax_addCourses(id = 0) {
    var inputText = $('.course').find(':text');
    var listText = "";
    for (var i = 0; i < inputText.length; i++) {
        var net = inputText.eq(i).val() + ";";
        listText += net;
    }
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "backMakeFrom",
            id: id,
            listText: listText
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0) {
                window.location.href = "./makeForm.php";
            } else if (data.content == 1) {
                alert(data.error);
            }
            // alert(data);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function ajax_addDiyQuestion() {
    var list = $(".active");
    var listText = "";
    for (var i = 0; i < list.length; i++) {
        var net = list[i].innerHTML + "|";
        listText += net;
    }
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "addCoursesInTemp",
            listText: listText
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0)
                window.location.href = "./makeProblem.php";
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function ajax_inputDiyQuestion(quId) {
    if (quId != false) {
    }
    // 问题名称
    var questionName = $(".fs36").find(":text").val();
    // 是否填写
    if ($(".switch.fr").hasClass("switch-close"))
        var isShow = 0;// 不填写
    else
        var isShow = 1;// 填写
    // 单选还是多选
    if ($(".bottom.fs36").hasClass("single"))
        var type = 1;// 单选
    else
        var type = 2;// 多选
    // 列出所有text
    var listText = "";
    var list_array = $(".lists.flex").find(":text");
    for (var i = 0; i < list_array.length; i++) {
        var net = list_array.eq(i).val() + "|";
        listText += net;
    }
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "inputDiyQuestion",
            quId: quId,
            quName: questionName,
            isShow: isShow,
            quType: type,
            liText: listText
        },
        success: function (data, textStatus, jqXHR) {
            // alert(data);
            if (data.content == 0)
                window.location.href = "./makeForm.php";
            else
                alert(data.content);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function ajax_selectDiyCourse(quId) {
    var list = $(".active");
    var listText = "";
    for (var i = 0; i < list.length; i++) {
        var net = list[i].innerHTML + "|";
        listText += net;
    }
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "addCoursesInTemp",
            listText: listText
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0)
                window.location.href = "./makeProblem.php?quId=" + quId;
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function ajax_saveForm() {
    $(".wait").html("请稍等...");
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "makeQrCodeNow",
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0)
                window.location.href = "./choose.php";
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function ajax_outPoster() {
    var image_bg = $(".bg").attr("src");
    var image_title = $(".title").attr("src");
    $(".wait").html("正在排队制作中，请勿返回");
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "outPoster",
            bg: image_bg,
            title: image_title
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0) {
                window.location.href = "./saveShare.php";
            }
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
//    ./saveShare.php
}

function ajax_addSession(id) {
    switch (id) {
        case 1:
            var image_bg = $(".swiper-container2 .swiper-slide.active").find("img").attr("src");
            $.ajax({
                url: "posts.php",
                type: "POST",
                async: true,
                timeOut: 50000,
                dataType: "json",
                data: {
                    type: "addSession",
                    bg: image_bg,
                    id: id
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.content == 0)
                        return 0;
                    else
                        alert(data.error);
                },
                error: function (xhr, textStatus) {
                    alert(xhr + textStatus);
                }
            });
            break;
        case 2:
            var image_title = $(".swiper-container1 .swiper-slide.active").find("img").attr("src");
            $.ajax({
                url: "posts.php",
                type: "POST",
                async: true,
                timeOut: 50000,
                dataType: "json",
                data: {
                    type: "addSession",
                    title: image_title,
                    id: id
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.content == 0)
                        return 0;
                    else
                        alert(data.error);
                },
                error: function (xhr, textStatus) {
                    alert(xhr + textStatus);
                }
            });
            break;
        default:
            alert("警告！");
    }
}

function ajax_cutComplete() {
    // 图片的宽高：
    var image_width = $(".cropper-view-box").find("img").css("width");
    var image_height = $(".cropper-view-box").find("img").css("height");
    // 框选的坐标：
    var cut_xy = $(".cropper-view-box").find("img").css("transform");
    // console.log(cut_xy);//matrix(1, 0, 0, 1, -41.1, -41.1)
    // 框选的宽高：
    var cut_width = $(".cropper-crop-box").css("width");
    var cut_height = $(".cropper-crop-box").css("height");
    alert(image_width + "|" + image_height + "|" + cut_xy + "|" + cut_width + "|" + cut_height);
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "cutCode",
            image_width: image_width,
            image_height: image_height,
            cut_xy: cut_xy,
            cut_width: cut_width,
            cut_height: cut_height
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0)
                window.location.href = "./make.php";
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });
}

function upload_form(uid, fid) {

    var yy = 0;
    $(".iText").each(function () {
        if ($(this).val() == "" || $(this).val().length > 20) {
            if (yy < 1){
                $(this).focus();
                yy = 1;
            }
            $(this).parent().removeClass("correct");
            $(this).parent().addClass("error");
        } else {
            $(this).parent().removeClass("error");
            $(this).parent().addClass("correct");
        }
    })

    // 文本输入框的名字
    var qu_name = $(".qu-name");
    var qu_text = $(".iText");
    // 单选的名字
    var radio_name = $(".on-name");
    var radio_text = $(".radio .active");
    // 多选的名字
    var box_name = $(".do-name");

    var all_push = [];
    var all_text = [];
    var all_radio = [];
    var all_box = [];
    
    for (var i = 0; i < qu_name.length; i++) {
        var add_text = [];
        // 文本输入框的问题：
        var qu_text_name = qu_name[i].innerHTML;
        // 答案
        var qu_text_an = qu_text[i].value;
        add_text.push(qu_text_name, qu_text_an);
        all_text.push(add_text);
    }
    for (var i = 0; i < radio_name.length; i++) {
        var add_radio = [];
        // 单选的问题
        var radio_text_name = radio_name[i].innerHTML;
        // 单选的答案：
        var radio_text_fix = radio_text[i].innerHTML;
        // 去除多于空格
        radio_text_fix = radio_text_fix.replace(/(^\s*)|(\s*$)/g, "");
        add_radio.push(radio_text_name, radio_text_fix)
        // console.log(on_name_text);
        all_radio.push(add_radio);
    }

    function selArr($ul) {
        var arr = [];
        var has = 0;
        $($ul + " li").each(function () {
            if ($(this).hasClass('active')) {
                has++;
                arr.push($(this).text())
            }
        })
        if (has < 1){
            $($ul + " li").parent().removeClass("correct");
            $($ul + " li").parent().addClass("error");
        } else {
            $($ul + " li").parent().addClass("correct");
            $($ul + " li").parent().removeClass("error");
        }
        return arr;
    }

    for (var i = 0; i < box_name.length; i++) {
        var add_box = [];
        // 多选名字
        var box_text_name = box_name[i].innerHTML;
        // 多选答案
        var box_name_text = [];
        var x = i + 1;
        box_name_text = selArr(".ab" + x);
        add_box.push(box_text_name, box_name_text);
        all_box.push(add_box);
    }
    all_push.push(all_text, all_radio, all_box);
    var all_push_json = JSON.stringify(all_push);
    if ($("li").hasClass("error")) {
        return 0;
    }
    console.log(all_push_json);
    $.ajax({
        url: "posts.php",
        type: "POST",
        async: true,
        timeOut: 50000,
        dataType: "json",
        data: {
            type: "uploadForm",
            form: all_push_json,
            cid: uid,
            fid: fid
        },
        success: function (data, textStatus, jqXHR) {
            if (data.content == 0)
            // $(".form").html("<center>报名成功√</center>");
                return 0;
            else
                alert(data.error);
        },
        error: function (xhr, textStatus) {
            alert(xhr + textStatus);
        }
    });

}

function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}