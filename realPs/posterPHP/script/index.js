 // // ajax:
 //            var ajax_val = $id.val().
 //            replace(/\[灰\]/g, "<gray>").replace(/\[\/灰\]/g, "</gray>").
 //            replace(/\[红\]/g, "<red>").replace(/\[\/红\]/g, "</red>").
 //            replace(/\[粗\]/g, "<bold>").replace(/\[\/粗\]/g, "</bold>").
 //            replace(/\[斜\]/g, "<italic>").replace(/\[\/斜\]/g, "</italic>").
 //            replace(/\[下划线\]/g, "<underline>").replace(/\[\/下划线\]/g, "</underline>").
 //            replace(/\[大\]/g, "<large>").replace(/\[\/大\]/g, "</large>")
 //            $.ajax({
 //                url: "posts.php",
 //                type: "POST",
 //                async: true,
 //                timeOut: 50000,
 //                dataType: "json",
 //                data: {
 //                    type: "addText",
 //                    num : id1,
 //                    text: ajax_val
 //                },
 //                success: function (data, textStatus, jqXHR) {
 //                    if (data.content == 0) {
 //                    }
 //                    else
 //                        alert(data.error);
 //                },
 //                error: function (xhr, textStatus) {
 //                    alert(xhr + textStatus);
 //                }
 //            })

/**
 *
 * @authors Your Name (you@example.org)
 * @date    2018-07-09 08:47:48
 * @version $Id$
 */

function adapte(){//适配
    var designWidth = 1080, rem2px = 100;
    document.documentElement.style.fontSize = window.innerWidth / designWidth * rem2px + "px";
}
$(window).resize(function(){
    adapte();
})
$(function(){
    var bH=$(document.body).height();
    console.log(bH)
    adapte();
    $(".cover").click(function(){//遮罩
        if(!$(this).hasClass("share-cover")){
            $(this).hide()
        }
    })
    $(".radio span").click(function(){//是、否按钮
        $(this).siblings("span").removeClass("active");
        $(this).addClass("active");
        if($(this).hasClass("b")){//自定义问题 单选-多选
            $(this).parents(".bottom").removeClass("single");
        }else if($(this).hasClass("a")){
            $(this).parents(".bottom").addClass("single");
        }
    })
    $(".bulid-form .checkbox>ul li").click(function(){//课程选择
        $(this).toggleClass("active");
    })
    $(".make-form .lists > li:not(:last-child)").click(function(){//生成报名表-内容选择
        $(this).toggleClass("active");
    })
    $(".switch").click(function(){ //自定义问题
        $(".make-problem .bottom").toggle();
        $(this).toggleClass("switch-close");
    })

    $(".make .edit-btn").click(function(){
        $(".make").addClass("edited");
        $(".edit").show()
        $(".swiper-container .swiper-slide").css({"width":"100%"});
    })
// 标题替换
    $(".edit-title").click(function(){
        $(".change").hide();
        $(".title-change").show();
    })
    $(".swiper-container1 .swiper-slide").click(function(){//标题选择-选中添加边框
        $(this).siblings().removeClass("active");
        $(this).addClass("active");

        var src = $(this).find("img").attr("src");
        $(".close").click(function(){//标题选择-选中添加边框
            $(".change").hide();
            if($(this).hasClass("close-ok")){//替换标题图片
                $(".title").attr("src",src)
            }
        })
    })

// 背景替换
    $(".edit-bg").click(function(){
        $(".change").hide();
        $(".bg-change").show();
    })
    $(".swiper-container2 .swiper-slide").click(function(){//标题选择-选中添加边框
        $(this).siblings().removeClass("active");
        $(this).addClass("active");

        var src = $(this).find("img").attr("src");
        $(".close").click(function(){//标题选择-选中添加边框
            $(".change").hide();
            if($(this).hasClass("close-ok")){//替换标题图片
                $(".bg").attr("src",src)
            }
        })
    })



    // 文字编辑
    $(".text-change .close").click(function(){//标题选择-选中添加边框
        $(".change").hide();
    })

    $(".edit-text1").unbind("click").click(function(){
        textEdit("#text-1","#text1")
    })
    $(".edit-text2").unbind("click").click(function(){
        textEdit("#text-2","#text2")
    })
    $(".edit-text3").unbind("click").click(function(){
        textEdit("#text-3","#text3")
    })
    function textEdit(id,id1){//文字编辑
        // if($(id).css("display") == "block" || $(id).css("display") == "inline-block"){
        //   return false;
        // }
        $("#text-1").hide();
        $("#text-2").hide();
        $("#text-3").hide();
        var $id = $(id);//textarea编辑框 id
        var $id1 = $(id1);//所要编辑的内容id
        $(".change").hide();//隐藏所有
        $(".text-change").show();//显示文本样式编辑框
        $id.show().empty();//显示并清空
        setStyle(document.querySelector(id));
        //内容处理
        var lis = $id1.children();
        //将所要编辑的内容 复制到 编辑框内
        var txt = [];
        for(var i =0 ;i<lis.length;i++){
            txt.push(lis.eq(i).text())
        }
        for(var j =0 ;j<txt.length;j++){
            $id.append(txt[j]+"\n");//
        }
        // 点击√时 复制编辑框的内容到
        $(".edit-ok").unbind("click").click(function(){
            // ajax: -------------------------------------------------------------
            var ajax_val = $id.val().
            replace(/\[灰\]/g, "<gray>").replace(/\[\/灰\]/g, "</gray>").
            replace(/\[红\]/g, "<red>").replace(/\[\/红\]/g, "</red>").
            replace(/\[粗\]/g, "<bold>").replace(/\[\/粗\]/g, "</bold>").
            replace(/\[斜\]/g, "<italic>").replace(/\[\/斜\]/g, "</italic>").
            replace(/\[下划线\]/g, "<line>").replace(/\[\/下划线\]/g, "</line>").
            replace(/\[大\]/g, "<large>").replace(/\[\/大\]/g, "</large>").
            replace(/\n/g,'<br />');
            $.ajax({
                url: "posts.php",
                type: "POST",
                async: true,
                timeOut: 50000,
                dataType: "json",
                data: {
                    type: "addText",
                    num : id1,
                    text: ajax_val
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.content == 0) {
                    }
                    else
                        alert(data.error);
                },
                error: function (xhr, textStatus) {
                    alert(xhr + textStatus);
                }
            })
            //--------------------------------------------------------------------
            var val = $id.val().
            replace(/\[灰\]/g, "<span style='color:gray;'>").replace(/\[\/灰\]/g, "</span>").
            replace(/\[红\]/g, "<span style='color:#d51717;'>").replace(/\[\/红\]/g, "</span>").
            replace(/\[粗\]/g, "<span style='font-weight:bold;'>").replace(/\[\/粗\]/g, "</span>").
            replace(/\[斜\]/g, "<span style='font-style: italic;'>").replace(/\[\/斜\]/g, "</span>").
            replace(/\[下划线\]/g, "<span style='text-decoration: underline;'>").replace(/\[\/下划线\]/g, "</span>").
            replace(/\[大\]/g, "<span style='font-size:large;'>").replace(/\[\/大\]/g, "</span>")
            var ary = val.split("\n");
            console.log(ary)
            $id1.empty();
            // console.log(ary)
            for(var k =0 ;k<ary.length;k++){
                $id1.append("<li>"+ary[k]+"</li>")
            }
            $id.hide();
            $(".change").hide();
        })
    }
    // 文字编辑-样式添加
    function setStyle($id){
        $(".style").unbind("click").click(function(event){
            var $event = $(event.target);
            var $class = $event.attr("class");
            switch ($class){
                case "gray":
                    getValue($id,'[灰][/灰]');
                    break;
                case "red":
                    getValue($id,'[红][/红]');
                    break;
                case "bold":
                    getValue($id,'[粗][/粗]');
                    break;
                case "italic":
                    getValue($id,'[斜][/斜]');
                    break;
                case "line":
                    getValue($id,'[下划线][/下划线]');
                    break;
                case "big":
                    getValue($id,'[大][/大]');
                    break;
                default:break;
            }
        })
    }
    // 文字编辑-光标-文本插入
    function getValue(obj,str) {
        // console.log(obj,str)
        if (document.selection) {
            var sel = document.selection.createRange();
            sel.text = str;
        } else if (typeof obj.selectionStart === 'number' && typeof obj.selectionEnd === 'number') {
            var startPos = obj.selectionStart,
                endPos = obj.selectionEnd,
                cursorPos = startPos,
                tmpStr = obj.value;
            obj.value = tmpStr.substring(0, startPos) + str + tmpStr.substring(endPos, tmpStr.length);
            cursorPos += str.length;
            obj.selectionStart = obj.selectionEnd = cursorPos;
        } else {
            obj.value += str;
        }
        obj.focus();
        var len = obj.value.length;
        if (document.selection) {
            var sel = obj.createTextRange();
            sel.moveStart('character',len);
            sel.collapse();
            sel.select();
        } else if (typeof obj.selectionStart == 'number' && typeof obj.selectionEnd == 'number') {
            obj.selectionStart = obj.selectionEnd = len;
        }
        return false
    }

    // 上传二维码
    $(".code .box").click(function(){
        // $(".box1").show();
        // $(".box1").css("z-index",9999999999999)
        // console.log(3321)
        // $("#image").show();
        // $(".cropper-container").show();
    })

})
