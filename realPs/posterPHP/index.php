<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>	
	<title>在线海报制作</title>
	<link rel="stylesheet" href="./css/base.css">
	<link rel="stylesheet" href="./css/index.css">
	<style>
	</style>
</head>
<body>
	<div class="index">
		<p class="b1"><a class="flex" href="./makeForm.php"><!-- <span>在线制作</span><span>报名表和海报</span> --></a></p>
		<p class="b2"><a class="flex" href="./code.php"><!-- 我有二维码，开始制作海报 --></a></p>
		<p class="b3"><a class="flex" href="./make.php?qr=false"><!-- 不需要二维码，开始制作海报 --></a></p>
	</div>
	
	<script src='./script/jquery.js'></script>
	<script src='./script/index.js'></script>
</body>
</html>