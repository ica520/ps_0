<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();


?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
        <title>课程</title>
        <link rel="stylesheet" href="./css/base.css">
        <link rel="stylesheet" href="./css/index.css">
    </head>
    <body>
    <div class="course">
        <ul class="lists flex">
            <?php
            if (isset($_GET['id']) && isset($_SESSION['temp'])) {
                $id = $_GET['id'];
                $temp = $_SESSION['temp'];
                $sql->courseWrite($id, $temp, $_SESSION['uid']);
            } else {
                ?>
                <script>
                    window.history.go(-1);
                </script>
                <?php
            }
            ?>
        </ul>
        <div class="btn">
            <button><a href="javascript:;" onclick="ajax_addCourses(<?= $_GET['id'] ?>)">提交</a>
            </button>
        </div>
    </div>
    </body>
    </html>
<?php
require_once './footer.php';