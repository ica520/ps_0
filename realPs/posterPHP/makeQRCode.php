<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/11
 * Time: 14:30
 */

session_start();
require_once './makePoster.class.php';

$allowedExts = array(
    "gif",
    "jpeg",
    "jpg",
    "png"
);

if ($_POST) {
    $makePoster = new makePoster();
    if (isset($_POST['upload-nonQRCode-content'])) {
        if (isset($_FILES["upload-nonQRCode-logo"])) {
            $logo_file = $_FILES["upload-nonQRCode-logo"];
            $logo_name = $logo_file["name"];
            $logo_size = $logo_file["size"];
            if ($logo_name != "") {
                if ($logo_file["error"]) {
                    exit('{"status":0,"url":"' . $logo_file["error"] . '"}');
                }
                if ($logo_size > (round(1024 * 1024, 2) * 12)) { //限制上传大小
                    exit('{"status":0,"url":"图片大小不能超过12M"}');
                }
                $type = strstr($logo_name, "."); //限制上传格式
                $temp_logo_name_array = explode(".", $logo_name);
                $ff_logo = end($temp_logo_name_array);
                if (!in_array($ff_logo, $allowedExts)) {
                    exit('{"status":2,"url":"图片格式不对！"}');
                }
                $logo_filename = $makePoster->fileReName();
                $logo_filename = $logo_filename . '.' . $ff_logo; //命名图片名称
                //上传路径
                $logo_path = "uploads/" . $logo_filename;
                move_uploaded_file($logo_file["tmp_name"], $logo_path);
                $sql->addTimeOutImages($_SESSION['uid'], $logo_path);
//            $size_logo = round($logo_size / 1024, 2); //转换成kb
//            $logo_im_size = getimagesize($logo_path);
//            exit('{"status":1,"url":"' . $logo_path . '","width":"' . $logo_im_size[0] . '","height":"' . $logo_im_size[1] . '","size":"' . $size_logo . '"}');
            }
        } else {
            $logo_path = '';
        }
        $QRCode_path = $makePoster->makeQRCode($_POST['upload-nonQRCode-content'], 3, $logo_path);
        exit('{"status":1,"url":"' . $QRCode_path . '"}');
    }

}
