<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/13
 * Time: 13:53
 */

require_once './header.php';
require_once './core/sql.php';
require_once './makePoster.class.php';
$sql = new SqlS();
$makePoster = new makePoster();

if (isset($_POST['type'])) {
    $type = $_POST['type'];
    switch ($type) {
        case 'addCoursesInTemp':
            if (isset($_POST['listText'])) {
                $_SESSION['temp'] = $_POST['listText'];
                exit('{"content":"0"}');
            } else {
                exit('{"content":"1","error":"no.1"}');
            }
            break;
        case 'backMakeFrom':
            $sql->addCourses($_POST['listText'], $_POST['id'], $_SESSION['uid']);
//            exit('111');
            exit('{"content":"0"}');
            break;
        case 'inputDiyQuestion':
            if ($sql->inputDiyQuestion($_POST['quId'], $_POST['quName'], $_POST['isShow'], $_POST['quType'], $_POST['liText'], $_SESSION['uid']))
                exit('{"content":"0"}');
            else
                exit('{"content":"1"}');
            break;
        case 'makeQrCodeNow':
            $sql->makeQrCodeNow($_SESSION['uid'], $_SESSION['temp'], $_SESSION['fid']);
            $host = $_SERVER['HTTP_HOST'];
            $uid = $_SESSION['uid'];
            $fid = $_SESSION['fid'];
            $_SESSION['code'] = $makePoster->makeQRCode($_SESSION['uid'], "http://$host/form.php?uid=$uid&fid=$fid");
            exit('{"content":"0"}');
            break;
        case 'outPoster':
            $_SESSION['image-bg'] = $_POST['bg'];
            $_SESSION['image-title'] = $_POST['title'];
            $su = require_once './poster.php';
            if ($su == 'success')
                exit('{"content":"0"}');
            else
                exit('{"content":"1"}');
            break;
        case 'addSession':
            if (isset($_POST['id'])) {
                switch ($_POST['id']) {
                    case '1':
                        // 背景：
                        $_SESSION['image-bg-id'] = $sql->changeDeImageTextToId($_POST['bg'], 1);
                        exit('{"content":"0"}');
                        break;
                    case '2':
                        // 标题
                        $_SESSION['image-title-id'] = $sql->changeDeImageTextToId($_POST['title'], 2);
                        exit('{"content":"0"}');
                        break;
                    default:
                        exit('{"content":"1"}');
                }
            } else {
                exit('{"content":"1"}');
            }
            break;
        case 'addText':
            switch ($_POST['num']) {
                case '#text1':
                    $_SESSION['text1'] = $_POST['text'];
                    exit('{"content":"0"}');
                    break;
                case '#text2':
                    $_SESSION['text2'] = $_POST['text'];
                    exit('{"content":"0"}');
                    break;
                case '#text3':
                    $_SESSION['text3'] = $_POST['text'];
                    exit('{"content":"0"}');
                    break;
                default:
                    exit('{"content":"1"}');
            }
            break;
        case 'cutCode':
            $_SESSION['code'] = $makePoster->cutCode($_POST['image_width'], $_POST['image_height'], $_POST['cut_xy'], $_POST['cut_width'], $_POST['cut_height'], $_SESSION['image-code']);
            exit('{"content":"0"}');
            break;
        case 'uploadForm':
            $form_array = json_decode($_POST['form']);
            $sql->addForm($form_array, $_POST['cid'], $_POST['fid']);
            exit('{"content":"0"}');
            break;
        default :
    }
}