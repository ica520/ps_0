<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
    <title>自定义问题</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
<div class="make-problem">
    <div class="top">
        <?php
        if (isset($_GET['quId'])) { //如果是设定了问题id
        $quId = $_GET['quId'];
        list($quName, $quShow, $quType, $quModes) = $sql->seSelectDiyQuCo($quId);
        var_dump($quName);
        $quModes_array_a = explode('|', $quModes);
        $quModes_array_b = explode(';', $quModes_array_a[1]);
        $count_quModes = count($quModes_array_b);
        // 删除所有空白
        for ($i = 0; $i < $count_quModes; $i++) {
            $key = array_search('', $quModes_array_b);
            if ($key !== false)
                array_splice($quModes_array_b, $key, 1);
        }
        $count_quModes_array_b = count($quModes_array_b);
        ?>
        <p class="fs36"><span>输入问题</span>
            <input type="text" value="<?= $quName ?>"/>
        </p>
        <p class="clearfix">
            <span class="fl fs36">是否填写</span>
            <?php
            if ($quShow == 1) {
            // 填写打开
            ?>
            <span class="switch fr"></span>
        </p>
    </div>
    <?php
    if ($quType == 1){
    // 打开单选
    ?>
    <div class="bottom single fs36">
        <span>填写方式</span>
        <div class="radio">
            <span class="active a">单选</span>
            <span class="b">多选</span>
        </div>
        <?php
        }elseif ($quType == 2){
        // 打开多选
        ?>
        <div class="bottom fs36">
            <span>填写方式</span>
            <div class="radio">
                <span class="a">单选</span>
                <span class="active b">多选</span>
            </div>
            <?php
            }
            } elseif ($quShow == 0) {
            // 填写关闭
            ?>
            <span class="switch fr switch-close"></span>
            </p>
        </div>
    <?php
    if ($quType == 1){
    // 关闭单选
    ?>
        <div class="bottom single fs36" style="display: none;">
            <span>填写方式</span>
            <div class="radio">
                <span class="active a">单选</span>
                <span class="b">多选</span>
            </div>
            <?php
            }elseif ($quType == 2){
            // 关闭多选
            ?>
            <div class="bottom fs36" style="display: none;">
                <span>填写方式</span>
                <div class="radio">
                    <span class="a">单选</span>
                    <span class="active b">多选</span>
                </div>
                <?php
                }
                }
                ?>
                <ul class="lists flex">
                    <?php
                    for ($i = 0; $i < $count_quModes_array_b; $i++) {

                        ?>
                        <li>
                            <span class="">
                                <?= $i + 1 ?>
                            </span>
                            <input type="text" placeholder="请填写可选答案" value="<?= $quModes_array_b[$i] ?>">
                        </li>
                        <?php
                    }
                    for ($i = 0; $i < (10 - $count_quModes_array_b); $i++) {
                        ?>
                        <li>
                            <span>
                                <?= $count_quModes_array_b + $i + 1 ?>
                            </span>
                            <input type="text" placeholder="请填写可选答案">
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <?php
            } else {
            // 如果没有设定问题id
            ?>
            <p class="fs36"><span>输入问题</span>
                <input type="text"/>
            </p>
            <p class="clearfix"><span class="fl fs36">是否填写</span><span class="switch fr"></span></p>
        </div>
        <div class="bottom single fs36">
            <span>填写方式</span>
            <div class="radio">
                <span class="active a">单选</span>
                <span class="b">多选</span>
            </div>
            <ul class="lists flex">
                <li><span class="">1</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>2</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>3</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>4</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>5</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>6</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>7</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>8</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>9</span><input type="text" placeholder="请填写可选答案"></li>
                <li><span>10</span><input type="text" placeholder="请填写可选答案"></li>
            </ul>
        </div>
    <?php
    }
    ?>
        <div class="btn">
            <?php
            if (isset($_GET['quId'])) {
                // 有问题id。那么修改问题内容
                ?>
                <button class="btn" onclick="ajax_inputDiyQuestion(<?= $_GET['quId'] ?>)">
                    <a href="javascript:;">生成报名表</a>
                </button>
                <?php
            } else {
                // 没有问题id，那么添加问题
                ?>
                <button class="btn" onclick="ajax_inputDiyQuestion(false)">
                    <a href="javascript:;">生成报名表</a>
                </button>
                <?php
            }
            ?>
        </div>
    </div>
</body>
</html>
<?php
require_once './footer.php';
?>