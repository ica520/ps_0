-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2018-07-13 09:11:38
-- 服务器版本： 5.7.14
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poster_system`
--
CREATE DATABASE IF NOT EXISTS `poster_system` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `poster_system`;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_default_problems`
--

CREATE TABLE `tbl_default_problems` (
  `pk_DefaultProblems` int(11) UNSIGNED NOT NULL,
  `DePr_Name` varchar(60) NOT NULL,
  `DePr_Show` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DePr_Type` int(11) NOT NULL,
  `DePr_Mode` varchar(255) DEFAULT '',
  `DePr_DefaultText` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_default_problems`
--

INSERT INTO `tbl_default_problems` (`pk_DefaultProblems`, `DePr_Name`, `DePr_Show`, `DePr_Type`, `DePr_Mode`, `DePr_DefaultText`) VALUES
(1, '家长姓名', 1, 3, '5', '请填写家长姓名'),
(2, '家长手机', 1, 6, '11', '请填写家长手机'),
(3, '学生姓名', 1, 3, '5', '请填写学生姓名'),
(4, '性别', 1, 1, '2|男;女;', ''),
(5, '所在学校', 1, 3, '20', '请填写所在学校'),
(6, '出生日期', 1, 5, '', ''),
(7, '爱好', 1, 3, '25', '请填写爱好'),
(8, '家庭地址', 1, 3, '50', '请填写家庭地址'),
(9, '年龄', 1, 6, '100', ''),
(10, '是否学过', 1, 1, '2|是;否;', ''),
(11, '课程', 1, 2, '10|', '请输入课程名称');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_diy_problems`
--

CREATE TABLE `tbl_diy_problems` (
  `pk_DiyProblems` int(11) UNSIGNED NOT NULL,
  `DiPr_CreateUid` int(10) UNSIGNED NOT NULL,
  `DiPr_Name` varchar(60) NOT NULL,
  `DiPr_IsUsing` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `DiPr_Type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DiPr_Answers` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_table_chooses`
--

CREATE TABLE `tbl_table_chooses` (
  `pk_TableChooses` int(11) UNSIGNED NOT NULL,
  `TaCh_UId` int(11) UNSIGNED NOT NULL,
  `TaCh_DePrId` varchar(255) NOT NULL,
  `TaCh_DiPrId` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_users`
--

CREATE TABLE `tbl_users` (
  `pk_Users` int(11) UNSIGNED NOT NULL,
  `User_Account` varchar(60) NOT NULL,
  `User_PassWord` varchar(60) NOT NULL,
  `User_UserName` varchar(20) NOT NULL,
  `User_rank` int(11) UNSIGNED NOT NULL,
  `User_email` varchar(255) NOT NULL,
  `User_CreateTime` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `User_LastLogin` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `User_LoginTries` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `User_IsLogin` tinyint(1) NOT NULL DEFAULT '0',
  `User_ClockTime` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_users`
--

INSERT INTO `tbl_users` (`pk_Users`, `User_Account`, `User_PassWord`, `User_UserName`, `User_rank`, `User_email`, `User_CreateTime`, `User_LastLogin`, `User_LoginTries`, `User_IsLogin`, `User_ClockTime`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', 10, 'admin@admin.com', 1528940046, 1530513684, 0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_default_problems`
--
ALTER TABLE `tbl_default_problems`
  ADD PRIMARY KEY (`pk_DefaultProblems`);

--
-- Indexes for table `tbl_diy_problems`
--
ALTER TABLE `tbl_diy_problems`
  ADD PRIMARY KEY (`pk_DiyProblems`);

--
-- Indexes for table `tbl_table_chooses`
--
ALTER TABLE `tbl_table_chooses`
  ADD PRIMARY KEY (`pk_TableChooses`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`pk_Users`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `tbl_default_problems`
--
ALTER TABLE `tbl_default_problems`
  MODIFY `pk_DefaultProblems` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- 使用表AUTO_INCREMENT `tbl_diy_problems`
--
ALTER TABLE `tbl_diy_problems`
  MODIFY `pk_DiyProblems` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `tbl_table_chooses`
--
ALTER TABLE `tbl_table_chooses`
  MODIFY `pk_TableChooses` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `pk_Users` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
