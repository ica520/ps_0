-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2018-07-02 16:22:51
-- 服务器版本： 5.7.14
-- PHP Version: 5.6.25/7.0.10/7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poster_system`
--
CREATE DATABASE IF NOT EXISTS `poster_system` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `poster_system`;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_users` 用户表
--

CREATE TABLE `tbl_users` (
  `pk_users_id` int(11) UNSIGNED NOT NULL,
  `us_Account` varchar(60) NOT NULL,  -- 账号
  `us_PassWord` varchar(60) NOT NULL, -- 密码
  `us_UserName` varchar(20) NOT NULL, -- 用户名
  `us_Rank` int(11) UNSIGNED NOT NULL,  -- 用户等级
  `us_Email` varchar(255) NOT NULL, -- 用户邮箱
  `us_CreateTime` int(11) UNSIGNED NOT NULL DEFAULT '0',  -- 创建时间
  `us_LastLoginTime` int(11) UNSIGNED NOT NULL DEFAULT '0', -- 最后登录时间
  `us_LoginTries` tinyint(4) UNSIGNED NOT NULL DEFAULT '0', -- 尝试登录时间
  `us_isLogin` tinyint(1) NOT NULL DEFAULT '0', -- 是否登录
  `us_ClockTime` int(11) NOT NULL DEFAULT '0' -- 锁定时间
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_users` 用户数据
--

INSERT INTO `tbl_users` (`pk_users_id`, `us_Account`, `us_PassWord`, `us_UserName`, `us_Rank`, `us_Email`, `us_CreateTime`, `us_LastLoginTime`, `us_LoginTries`, `us_isLogin`, `us_ClockTime`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', '10', 'admin@admin.com', 1528940046, 1530513684, 0, 1, 0);

--
-- 表的结构 `tbl_questions` 问题设置
--

CREATE TABLE `tbl_questions` (
  `pk_questions_id` int(11) UNSIGNED NOT NULL,
  `qs_CreateUid` int(11) UNSIGNED NOT NULL DEFAULT 0, -- 创建者uid默认0
  `qs_Name` varchar(60) NOT NULL,  -- 问题名称
  `qs_isShow` tinyint(1) UNSIGNED NOT NULL DEFAULT '0', -- 问题是否启用 0.未启用 1.启用
  `qs_Type` int(11) NOT NULL,  -- 问题类型 1.单选 2.多选 3.单行文字填写 4.多行文字填写 5.日期填写 6.数字填写
  `qs_Modes` varchar(255) DEFAULT '', -- 问题模式 单选的内容（数目|默认问题1;问题2;问题3;...;），多选的内容(数目|默认问题1;问题2;问题3;...;），单行文字填写的限制个数， 多行文字填写的限制个数（行数|个数）， 数字填写的最大值
  `qs_DefaultText` varchar(255) DEFAULT ''  -- 默认背景字
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_questions`
--

INSERT INTO tbl_questions (`pk_questions_id`, `qs_CreateUid`, `qs_Name`, `qs_isShow`, `qs_Type`, `qs_Modes`, `qs_DefaultText`) VALUES
(1, '1', '家长姓名', '1', '3', '5','请填写家长姓名'),
(2, '1', '家长手机', '1', '6', '11', '请填写家长手机'),
(3, '1', '学生姓名', '1', '3', '5', '请填写学生姓名'),
(4, '1', '性别', '1', '1', '2|男;女;', ''),
(5, '1', '所在学校', '1', '3', '20', '请填写所在学校'),
(6, '1', '出生日期', '1', '5', '', ''),
(7, '1', '爱好', '1', '3', '25', '请填写爱好'),
(8, '1', '家庭地址', '1', '3', '50', '请填写家庭地址'),
(9, '1', '年龄', '1', '6', '100', ''),
(10, '1', '是否学过', '1', '1', '2|是;否;', ''),
(11, '1', '课程', '1', '2', '10|', '请填写课程'),
(12, '2', '困难', '1', '1', '|有;无;','');

--
-- 表的结构 `tbl_tables` 创建的表格 就是把问题都复制过来备份一份固定住
--

CREATE TABLE `tbl_tables` (
  `pk_tables_id` int(11) UNSIGNED NOT NULL,
  `ts_UserId` int(11) UNSIGNED NOT NULL,  -- 创建者用户ID
  `ts_TaId` int(11) UNSIGNED NOT NULL, -- 创建的表格ID
  `ts_QuName` varchar(255) NOT NULL, -- 问题名称
  `ts_QuType` int(11) UNSIGNED NOT NULL,  -- 问题类型
  `ts_QuModes` varchar(255) NOT NULL,  -- 问题内容
  `ts_QuBaText` varchar(255) NOT NULL DEFAULT ''  -- 默认背景字
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 表的结构 `tbl_table_content` 填写的表格内容
--

CREATE TABLE `tbl_table_content` (
  `pk_table_content_id` int(11) UNSIGNED NOT NULL,
  `tc_UserId` int(11) UNSIGNED NOT NULL,  -- 用户ID
  `tc_CId` int(11) UNSIGNED NOT NULL, -- 创建表格的uid
  `tc_TaId` int(11) UNSIGNED NOT NULL, -- 填写的表格ID
  `tc_QuId` int(11) UNSIGNED NOT NULL,  -- 问题id
  `tc_QuType` int(11) UNSIGNED NOT NULL,  -- 问题类型
  `tc_QuName` varchar(255) NOT NULL, -- 填写的问题名称
  `tc_QuAnswer` varchar(255)  -- 填写的问题答案
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 表的结构 `tbl_question_content` 特殊问题，仅用于默认问题和多选问题
--

CREATE TABLE `tbl_question_content` (
  `pk_question_content_id` int(11) UNSIGNED NOT NULL,
  `qc_userId` int(11) UNSIGNED NOT NULL,  -- 用户ID
  `qc_QuId` int(11) UNSIGNED NOT NULL,  -- 问题id
  `qc_QuContent` varchar(255) NOT NULL -- 填写的内容
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_question_content`
--

INSERT INTO tbl_question_content (`pk_question_content_id`, `qc_userId`, `qc_QuId`, `qc_QuContent`) VALUES
  (1, 2, 11, '面对面英语辅导'),
  (2, 2, 11, '面对面数学辅导'),
  (3, 2, 11, '语文写作');

--
-- 表的结构 `tbl_image_recycle_bin` 图片回收站，清理存放了14天的垃圾图片
--

CREATE TABLE `tbl_image_recycle_bin` (
  `pk_image_recycle_bin_id` int(11) UNSIGNED NOT NULL,
  `qrb_userId` int(11) UNSIGNED NOT NULL,  -- 创建图片用户ID
  `qrb_imageUrl` varchar(255) NOT NULL DEFAULT '',  -- 图片链接
  `qrb_createTime` int(11) NOT NULL, -- 创建时间
  `qrb_deleteTime` int(11) NOT NULL DEFAULT '999999999' -- 删除日期-一般30天
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 表的结构 `tbl_default_images` 默认图片管理员设定的可选图片如背景、标题
--

CREATE TABLE `tbl_default_images` (
  `pk_default_images_id` int(11) UNSIGNED NOT NULL,
  `di_imType` int(11) UNSIGNED NOT NULL,  -- 创建图片类型 1 背景 2 标题
  `di_imUrl_a` varchar(255) NOT NULL DEFAULT '',  -- 前端图片链接
  `di_imUrl_b` varchar(255) NOT NULL DEFAULT '',   -- 后端图片链接
  `di_diy_x` int(11) NOT NULL DEFAULT 0,
  `di_diy_y` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_default_images`
--

INSERT INTO tbl_default_images (`pk_default_images_id`, `di_imType`, `di_imUrl_a`, `di_imUrl_b`, `di_diy_x`, `di_diy_y`) VALUES
  (1, 1, './img/bg1.jpg', './images/bg1.png', 0, 0),
  (2, 1, './img/bg2.jpg', './images/bg2.png', 0, 0),
  (3, 1, './img/bg3.jpg', './images/bg3.png', 0, 0),
  (4, 2, './img/title1.png', './images/title1.png', 111, 56),
  (5, 2, './img/title2.png', './images/title2.png', 212, 133),
  (6, 2, './img/title3.png', './images/title3.png', 186, -68);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`pk_users_id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE tbl_questions
  ADD PRIMARY KEY (`pk_questions_id`);

--
-- Indexes for table `tbl_tables`
--
ALTER TABLE tbl_tables
  ADD PRIMARY KEY (`pk_tables_id`);

--
-- Indexes for table `tbl_table_content`
--
ALTER TABLE `tbl_table_content`
  ADD PRIMARY KEY (`pk_table_content_id`);

--
-- Indexes for table `tbl_question_content`
--
ALTER TABLE `tbl_question_content`
  ADD PRIMARY KEY (`pk_question_content_id`);

--
-- Indexes for table `tbl_question_content`
--
ALTER TABLE `tbl_image_recycle_bin`
  ADD PRIMARY KEY (`pk_image_recycle_bin_id`);

--
-- Indexes for table `tbl_default_images`
--
ALTER TABLE `tbl_default_images`
  ADD PRIMARY KEY (`pk_default_images_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `pk_users_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_questions`
--
ALTER TABLE tbl_questions
  MODIFY `pk_questions_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_tables`
--
ALTER TABLE tbl_tables
  MODIFY `pk_tables_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_table_content`
--
ALTER TABLE `tbl_table_content`
  MODIFY `pk_table_content_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_question_content`
--
ALTER TABLE `tbl_question_content`
  MODIFY `pk_question_content_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_question_content`
--
ALTER TABLE `tbl_image_recycle_bin`
  MODIFY `pk_image_recycle_bin_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- 使用表AUTO_INCREMENT `tbl_question_content`
--
ALTER TABLE `tbl_default_images`
  MODIFY `pk_default_images_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
