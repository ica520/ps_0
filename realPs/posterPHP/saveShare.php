<?php
require_once './header.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
    <title>保存/分享</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
<div class="save-share">
    <?php
    if (isset($_SESSION['uid']) && isset($_SESSION['fid']) && $_SESSION['make-done-poster']) {
        ?>
        <img src="<?= $_SESSION['make-done-poster'] ?>" alt="">
        <?php
    } else {
        ?>
        <img src="./img/img1.jpg" alt="">
        <?php
    }
    ?>
    <div class="btns">
        <button class="save"><a href="./download.php?filename=<?= $_SESSION['make-done-poster'] ?>">保存到手机</a></button>
        <p>保存到手机（jpg格式，可自己发送到朋友圈或好友）</p>
        <button class="share-link"><a href="./share.php?uid=<?= $_SESSION['uid'] ?>&fid=<?= $_SESSION['fid'] ?>">保存生成链接分享</a></button>
        <p>生成链接分享（以网页链接形式分享到朋友圈或好友）</p>
    </div>
</div>

<script src='./script/jquery.js'></script>
<script src='./script/index.js'></script>
</body>
</html>