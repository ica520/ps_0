<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();
//var_dump($_SESSION);

$list_bg_ims = $sql->listDeImage(1);
$list_title_ims = $sql->listDeImage(2);
if ($list_bg_ims != array()) {
    $list_bg_ims_a = $list_bg_ims[0];
    $count_bg = count($list_bg_ims_a);
} else {
    echo 'no bg';
    $list_bg_ims_a = array('./img/bg1.jpg', './img/bg2.jpg', './img/bg3.jpg');
    $count_bg = 3;
}
if ($list_title_ims != array()) {
    $list_title_ims_a = $list_title_ims[0];
    $count_title = count($list_title_ims_a);
} else {
    echo 'no title';
    $list_title_ims_a = array('./img/title1.png', './img/title2.png', './img/title3.png');
    $count_title = 3;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
    <title>制作海报</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./css/swiper.min.css">
</head>
<body>
<div class="make fs36 clearfix">
    <div class="make-box">
        <?php
        if (isset($_SESSION['image-bg-id'])) {
            ?>
            <img class="bg" src="<?= $list_bg_ims_a[$_SESSION['image-bg-id']] ?>" alt="bg">
            <?php
        } else {
            ?>
            <img class="bg" src="<?= $list_bg_ims_a[0] ?>" alt="bg">
            <?php
        }
        ?>
        <div class="top">
            <?php
            if (isset($_SESSION['image-logo'])) {
                ?>
                <img class="logo" src="<?= $_SESSION['image-logo'] ?>" alt="logo">
                <?php
            } else {
                ?>
                <img class="logo" src="./img/logo.jpg" alt="logo">
                <?php
            }
            if (isset($_SESSION['image-title-id'])) {
                ?>
                <img class="title" src="<?= $list_title_ims_a[$_SESSION['image-title-id']] ?>" alt="">
                <?php
            } else {
                ?>
                <img class="title" src="<?= $list_title_ims_a[0] ?>" alt="">
                <?php
            }
            ?>
            <form id="upload-logo" action="upload.php" method="post" enctype="multipart/form-data">
                <div class="edit edit-logo">
                    <input id="submit-logo" name="image-logo" type="file">
                    <span>logo上传</span>
                </div>
            </form>
            <div class="edit edit-title">
                <span>标题替换</span>
            </div>
            <div class="edit edit-bg">
                <span>背景替换</span>
            </div>
        </div>
        <div class="content">
            <div class="edit edit-text1">
                <span>文字编辑</span>
            </div>
            <div class="edit edit-text2">
                <span>文字编辑</span>
            </div>
            <ul id="text1">
                <li>内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，</li>
            </ul>
            <ul class="flex fs42" id="text2">
                <li><p>暑期活动课程<span style="font-size:large;color: #d51717">8888</span>元/12节课时</p></li>
                <li><p>团购价<span style="font-size:large;color: #d51717">588</span>元/12节课时 （五人成团）</p></li>
            </ul>
            <div class="edit edit-pic">
                <span>图片版式</span>
            </div>
            <div class="img">
                <div class="img1">
                    <div>
                        <form id="upload-img1" action="upload.php" method="post" enctype="multipart/form-data">
                            <div class="url-img1">
                                <?php
                                if (isset($_SESSION['image-content1'])) {
                                    ?>
                                    <img src="<?= $_SESSION['image-content1'] ?>" alt="content1">
                                    <?php
                                }
                                ?>
                            </div>
                            <!-- <img src="./img/content1.jpg" alt="content1"> -->
                            <input id="submit-img1" name="image-content1" type="file">
                            <span class="text-img1">点击上传图片</span>
                        </form>
                    </div>
                </div>
                <div class="img2">
                    <div>
                        <form id="upload-img2" action="upload.php" method="post" enctype="multipart/form-data">
                            <div class="url-img2">
                                <?php
                                if (isset($_SESSION['image-content2'])) {
                                    ?>
                                    <img src="<?= $_SESSION['image-content2'] ?>" alt="content2">
                                    <?php
                                }
                                ?>
                            </div>
                            <!-- <img src="./img/content2.jpg" alt="content2"> -->
                            <input id="submit-img2" name="image-content2" type="file">
                            <span class="text-img2">点击上传图片</span>
                        </form>
                    </div>
                </div>
            </div>
            <img class="bg-icon" src="./img/bg-icon.png" alt="">
        </div>
        <div class="bottom  flex">
            <div class="edit edit-text3">
                <span>文字编辑</span>
            </div>
            <ul class="bottom-left" id="text3">
                <li><span class="b">招生对象:</span><span>内容内容内容</span></li>
                <li><span class="b">咨询热线:</span><span>0574-87993919/87918150</span></li>
                <li><span class="b">地址:</span><span>宁波市海曙区丽园北路668号美丽园大厦 602</span></li>
            </ul>
            <?php
            if (isset($_GET['qr'])) {
                if ($_GET['qr'] == false) {
                    unset($_SESSION['code']);
                }
            } else if (isset($_SESSION['code'])) {
                ?>
                <div class="bottom-right flex ">
                    <div>
                        <img src="<?= $_SESSION['code'] ?>" alt="微信二维码">
                    </div>
                    <p><span>微信扫码了解更多详情</span></p>
                </div>
                <?php
            } else {
                ?>
                <div class="bottom-right flex ">
                    <div>
                        <img src="./img/wxCode.jpg" alt="微信二维码">
                        <!--                <img src="./img/wxCode.jpg" alt="微信二维码">-->
                    </div>
                    <p><span>微信扫码了解更多详情</span></p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="title-change change"><!-- 标题替换 -->
        <div class="aa close"><img src="./img/icon3.png" alt=""></div>
        <div class="swiper-container1 swiper-container-horizontal swiper-container-android">
            <div class="swiper-wrapper">
                <?php
                if ($count_title > 0) {
                    for ($m = 0; $m < $count_title; $m++) {
                        if (isset($_SESSION['image-title-id'])) {
                            if ($_SESSION['image-title-id'] == $m) {
                                switch ($m) {
                                    case '0':
                                        ?>
                                        <div class="swiper-slide  swiper-slide-active active">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    case '1':
                                        ?>
                                        <div class="swiper-slide swiper-slide-next active">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    default:
                                        ?>
                                        <div class="swiper-slide active">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                    <?php
                                }
                            } else {
                                switch ($m) {
                                    case '0':
                                        ?>
                                        <div class="swiper-slide  swiper-slide-active">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    case '1':
                                        ?>
                                        <div class="swiper-slide swiper-slide-next">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    default:
                                        ?>
                                        <div class="swiper-slide">
                                            <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                        </div>
                                    <?php
                                }
                            }
                        } else {
                            switch ($m) {
                                case '0':
                                    ?>
                                    <div class="swiper-slide  swiper-slide-active active">
                                        <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                    </div>
                                    <?php
                                    break;
                                case '1':
                                    ?>
                                    <div class="swiper-slide swiper-slide-next">
                                        <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                    </div>
                                    <?php
                                    break;
                                default:
                                    ?>
                                    <div class="swiper-slide">
                                        <img class="" src="<?= $list_title_ims_a[$m] ?>" alt="bg">
                                    </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="bb flex">
            <div class="close">
                <img src="./img/icon2.png" alt="">
            </div>
            <p class="fs36">标题替换</p>
            <div class="close close-ok" onclick="ajax_addSession(2)">
                <img src="./img/icon1.png" alt="">
            </div>
        </div>
    </div>
    <div class="bg-change change"><!-- 背景替换 -->
        <div class="aa close"><img src="./img/icon3.png" alt=""></div>
        <div class="swiper-container2 swiper-container-horizontal swiper-container-android">
            <div class="swiper-wrapper">
                <?php
                if ($count_bg > 0) {
                    for ($j = 0; $j < $count_bg; $j++) {
                        if (isset($_SESSION['image-bg-id'])) {
                            if ($_SESSION['image-bg-id'] == $j) {
                                switch ($j) {
                                    case '0':
                                        ?>
                                        <div class="swiper-slide  swiper-slide-active active">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    case '1':
                                        ?>
                                        <div class="swiper-slide swiper-slide-next active">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    default:
                                        ?>
                                        <div class="swiper-slide active">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                    <?php
                                }
                            } else {
                                switch ($j) {
                                    case '0':
                                        ?>
                                        <div class="swiper-slide  swiper-slide-active">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    case '1':
                                        ?>
                                        <div class="swiper-slide swiper-slide-next">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                        <?php
                                        break;
                                    default:
                                        ?>
                                        <div class="swiper-slide">
                                            <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                        </div>
                                    <?php
                                }
                            }
                        } else {
                            switch ($j) {
                                case '0':
                                    ?>
                                    <div class="swiper-slide  swiper-slide-active active">
                                        <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                    </div>
                                    <?php
                                    break;
                                case '1':
                                    ?>
                                    <div class="swiper-slide swiper-slide-next">
                                        <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                    </div>
                                    <?php
                                    break;
                                default:
                                    ?>
                                    <div class="swiper-slide">
                                        <img class="" src="<?= $list_bg_ims_a[$j] ?>" alt="bg">
                                    </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="bb flex">
            <div class="close">
                <img src="./img/icon2.png" alt="">
            </div>
            <p class="fs36">背景替换</p>
            <div class="close close-ok" onclick="ajax_addSession(1)">
                <img src="./img/icon1.png" alt="">
            </div>
        </div>
    </div>
    <div class="text-change change"><!-- 文字编辑 -->
        <div class="aa close"><img src="./img/icon3.png" alt=""></div>
        <div class="box">
            <div class="style flex">
                <button class="gray">T</button>
                <button class="red">T</button>
                <button class="bold">T</button>
                <button class="italic">T</button>
                <button class="line">T</button>
                <button class="big">T</button>
            </div>
            <!-- 	<ul id="text" contenteditable="true">
                </ul> -->
            <textarea name="" id="text-1" cols="5" rows="5" style="width:10.8rem;padding:10px"></textarea>
            <textarea name="" id="text-2" cols="5" rows="5" style="width:10.8rem;padding:10px"></textarea>
            <textarea name="" id="text-3" cols="5" rows="5" style="width:10.8rem;padding:10px"></textarea>
        </div>

        <div class="bb flex">
            <div class="close">
                <img src="./img/icon2.png" alt="">
            </div>
            <p class="fs36">文字编辑</p>
            <div class="close edit-ok edit-ok-1">
                <img src="./img/icon1.png" alt="">
            </div>
        </div>
    </div>
    <div class="btn save-btn">
        <button onclick="ajax_outPoster()"><a class="wait" href="javascript:void(0);">保存/分享</a></button>
    </div>
</div>

<script src='./script/jquery.js'></script>

<script src='./script/index.js'></script>
<script src="./script/swiper.js"></script>
<script src="./script/jquery.form.js"></script>
<script src="./script/app.js"></script>
<script>
    var swiper1 = new Swiper('.swiper-container1', {
        pagination: '.swiper-pagination',
        slidesPerView: 2,
        paginationClickable: true,
    });
    var swiper2 = new Swiper('.swiper-container2', {
        pagination: '.swiper-pagination',
        slidesPerView: 2,
        paginationClickable: true,
    });

</script>
<script>
    // var str="生产法洒出"
    // console.log(str.replace('生产', "jb51.net"))

</script>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function (e) {
        $("#submit-logo").change(function () {
            ajaxSubmieImage("#upload-logo", ".logo");
        });
        $("#submit-img1").change(function () {
            ajaxSubmieImage("#upload-img1", ".url-img1");
        });
        $("#submit-img2").change(function () {
            ajaxSubmieImage("#upload-img2", ".url-img2");
        });
    });
</script>