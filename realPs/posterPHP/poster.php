<?php
/**
 * Created by PhpStorm.
 * User: CaiJianling
 * Date: 2018/7/8
 * Time: 20:59
 */


if (!isset($_SESSION)) {
    exit('拒绝访问');
}
require_once './makePoster.class.php';
require_once './core/sql.php';
$makePoster = new makePoster();
$sql = new SqlS();
// 比率
const ADX = 28.346;

//创建背景：

$width = 1080;
$height = 1829;
$image_all = imagecreatetruecolor($width, $height);

//白色背景
$white = imagecolorallocate($image_all, 255, 255, 255);
$tsc = imagecolorallocatealpha($image_all, 255, 255, 255, 127);

imagefilledrectangle($image_all, 0, 0, $width, $height, $white);

//背景图片


//$bg_array = array('./images/bg1.png', './images/bg2.png', './images/bg3.png');
//$image_bg = imagecreatefrompng($bg_array[mt_rand(0, 2)]);

list($image_bg_url, $image_x, $image_y) = $sql->changeAImToBIm($_SESSION['image-bg'], 1);
$image_bg = $makePoster->createImageFromFile($image_bg_url);

//加上背景图片
imagecopy($image_all, $image_bg, $image_x, $image_y, 0, 0, $width, $height);

// 如果有logosesssion就用logo图片
if (isset($_SESSION['image-logo'])) {
//logo图片：
    $logo_size = getimagesize($_SESSION['image-logo']);
    $image_logo = $makePoster->createImageFromFile($_SESSION['image-logo']);
    imagecopy($image_all, $image_logo, 10, 20, 0, 0, $logo_size[0], $logo_size[1]);
}

//添加标题图片
list($image_big_title_url, $image_big_x, $image_big_y) = $sql->changeAImToBIm($_SESSION['image-title'], 2);
$image_big_title = $makePoster->createImageFromFile($image_big_title_url);
list($image_big_title_size_x, $image_big_title_size_y) = getimagesize($image_big_title_url);

imagecopy($image_all, $image_big_title, $image_big_x, $image_big_y, 0, 0, $image_big_title_size_x, $image_big_title_size_y);

//添加中心两张图片
//大的：
$image_c1bg1 = imagecreatefrompng('./images/c1bg1.png');
$image_c1bg1_size = getimagesize('./images/c1bg1.png');

$image_c1bg1_copy_width = 75;
$image_c1bg1_copy_height = 850;
imagecopy($image_all, $image_c1bg1, $image_c1bg1_copy_width, $image_c1bg1_copy_height, 0, 0, $image_c1bg1_size[0], $image_c1bg1_size[1]);

$image_c1bg2 = imagecreatefrompng('./images/c1bg2.png');
$image_c1bg2_size = getimagesize('./images/c1bg2.png');

$image_c1bg2_copy_width = 110;
$image_c1bg2_copy_height = 890;
imagecopy($image_all, $image_c1bg2, $image_c1bg2_copy_width, $image_c1bg2_copy_height, 0, 0, $image_c1bg2_size[0], $image_c1bg2_size[1]);

// 如果有se照片就用照片
if (isset($_SESSION['image-content1'])) {
//照片|816x434
    $file_pic1_string = $_SESSION['image-content1'];
    $image_pic1 = $makePoster->createImageFromFile($file_pic1_string);
    $pic1_bg = $makePoster->changeImagePixel(816, 434, $image_pic1, $width, $height);
    $image_pic1_rotate = $makePoster->rotateImage($pic1_bg, 1.51);
    imagecopy($image_all, $image_pic1_rotate, 110, 885, 0, 0, $width, $height);
}

//小的
$image_c2bg1 = imagecreatefrompng('./images/c2bg1.png');
$image_c2bg1_size = getimagesize('./images/c2bg1.png');

$image_c2bg1_copy_width = 752;
$image_c2bg1_copy_height = 1180;
imagecopy($image_all, $image_c2bg1, $image_c2bg1_copy_width, $image_c2bg1_copy_height, 0, 0, $image_c2bg1_size[0], $image_c2bg1_size[1]);

$image_c2bg2 = imagecreatefrompng('./images/c2bg2.png');
$image_c2bg2_size = getimagesize('./images/c2bg2.png');

$image_c2bg2_copy_width = 780;
$image_c2bg2_copy_height = 1200;
imagecopy($image_all, $image_c2bg2, $image_c2bg2_copy_width, $image_c2bg2_copy_height, 0, 0, $image_c2bg2_size[0], $image_c2bg2_size[1]);

// 如果有se照片就用照片
if (isset($_SESSION['image-content2'])) {
//照片|218x170
    $file_pic2_string = $_SESSION['image-content2'];
    $image_pic2 = $makePoster->createImageFromFile($file_pic2_string);
    $pic2_bg = $makePoster->changeImagePixel(218, 170, $image_pic2, $width, $height);
    $image_pic2_rotate = $makePoster->rotateImage($pic2_bg, -5.77);
    imagecopy($image_all, $image_pic2_rotate, 610, 1200, 0, 0, $width, $height);
}

if (isset($_SESSION['code'])) {
//二维码:
    $image_qrcode = imagecreatefrompng($_SESSION['code']);
    $image_qrcode_size = getimagesize($_SESSION['code']);

    $image_qrcode_copy_width = 844;
    $image_qrcode_copy_height = 1477;
    imagecopy($image_all, $image_qrcode, $image_qrcode_copy_width, $image_qrcode_copy_height, 0, 0, $image_qrcode_size[0], $image_qrcode_size[1]);
}

//微信扫码 了解更多详情
$wxsm_x = 845;
$wxsm_y = 1705;

$gray = imagecolorallocatealpha($image_all, 107, 107, 107, 0);

$fonts = array(
    '等线常规' => __DIR__ . '/fonts/Deng.ttf',
    '等线粗体' => __DIR__ . '/fonts/Dengb.ttf',
    '等线细体' => __DIR__ . '/fonts/Dengl.ttf'
);
imagettftext($image_all, 14, 0, $wxsm_x, $wxsm_y, $gray, $fonts['等线常规'], '微信扫码 了解更多详情');

//招生对象等：
//招生对象：内容内容内容内容内容内容 咨询热线：0574-87993919/87918150 地址：宁波市海曙区丽园北路668号
//$black = imagecolorallocatealpha($image_all, 0, 0, 0, 0);
//imagettftext($image_all, 22, 0, 186, 1520, $black, $fonts['等线粗体'], '招生对象：');
//imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520, $black, $fonts['等线常规'], '内容内容内容内容内容内容');
//imagettftext($image_all, 22, 0, 186, 1520 + 22 * 3, $black, $fonts['等线粗体'], '咨询热线：');
//imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 3, $black, $fonts['等线常规'], '0574-87993919/87918150');
//imagettftext($image_all, 22, 0, 186, 1520 + 22 * 6, $black, $fonts['等线粗体'], '地       址：');
//imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 6, $black, $fonts['等线常规'], '宁波市海曙区丽园北路668号');
//imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 8, $black, $fonts['等线常规'], '        美丽园大厦 602');

if (isset($_SESSION['text1'])) {
    $content = $_SESSION['text1'];

    $image_all = $makePoster->fixText($image_all, $content, 0);
}
if (isset($_SESSION['text2'])) {
$content = $_SESSION['text2'];

$image_all = $makePoster->fixText($image_all, $content, 1);
}
if (isset($_SESSION['text3'])) {
$content = $_SESSION['text3'];

$image_all = $makePoster->fixText($image_all, $content, 2);
}
//$content_array = mb_str_split($content);
//$count_content = count($content_array);
//
//$content_floor = floor($count_content);
//for ($j = 0; $j < ($count_content / 35); $j++) {
//    if ($count_content - $j * 35 > 35) {
//        for ($i = 0; $i < 35; $i++) {
//            $text_position_x = 115 + 24 * $i;
//            $text_position_y = 585 + 35 * $j;
//            imagettftext($image_all, 19, 0, $text_position_x, $text_position_y, $black, $fonts['等线常规'], $content_array[($j * 35 + $i)]);
//        }
//    } else {
//        for ($i = 0; $i < ($count_content - $j * 35); $i++) {
//            $msi = $count_content % 35;
//            $text_position_x = 115 + 24 * $i;
//            $text_position_x = $text_position_x + (35 / 2 * 24 - $msi / 2 * 24);
//            $text_position_y = 585 + 35 * $j;
//            imagettftext($image_all, 19, 0, $text_position_x, $text_position_y, $black, $fonts['等线常规'], $content_array[($j * 35 + $i)]);
//        }
//    }
//}


//imagettftext( $image_all, 19, 0, 115, 585, $black, $fonts['等线常规'], $content );


//底部横条
$image_csbar = imagecreatefrompng('./images/csbar.png');
$image_csbar_size = getimagesize('./images/csbar.png');
$image_logo_csbar_copy_width = 220;
$image_logo_csbar_copy_height = 1428;
imagecopy($image_all, $image_csbar, $image_logo_csbar_copy_width, $image_logo_csbar_copy_height, 0, 0, $image_csbar_size[0], $image_csbar_size[1]);

//暑期活动课程888元/12节课时
//团购价588元/12节课时（五人成团）
//$red_deep = imagecolorallocatealpha($image_all, 213, 23, 23, 0);
//imagettftext($image_all, 30, 0, 218, 750, $black, $fonts['等线粗体'], '暑期活动课程');
//$ontcount1 = count(mb_str_split('暑期活动课程'));
//imagettftext($image_all, 45, 0, 218 + 30 * 1.35 * $ontcount1, 750, $red_deep, $fonts['等线粗体'], ' 888');//465
//$ontcount2 = count(mb_str_split(' 888'));
//imagettftext($image_all, 30, 0, 218 + 30 * 1.35 * $ontcount1 + 45 * 0.75 * $ontcount2, 750, $black, $fonts['等线粗体'], '元/12节课时');//590
//imagettftext($image_all, 30, 0, 218, 830, $black, $fonts['等线粗体'], '团购价');
//imagettftext($image_all, 45, 0, 340, 830, $red_deep, $fonts['等线粗体'], '588');
//imagettftext($image_all, 30, 0, 460, 830, $black, $fonts['等线粗体'], '元/12节课时（五人成团）');

//红球
$image_ball = imagecreatefrompng('./images/ball.png');
$image_ball_size = getimagesize('./images/ball.png');
imagecopy($image_all, $image_ball, 170, 725, 0, 0, $image_ball_size[0], $image_ball_size[1]);
imagecopy($image_all, $image_ball, 170, 805, 0, 0, $image_ball_size[0], $image_ball_size[1]);

//header("content-type:text/html;charset=utf-8");
mb_internal_encoding('UTF-8');
//header('Content-Type:image/jpeg');
$image_url = './posters/' . $_SESSION['uid'] . '-' . $_SESSION['fid'] . '.jpg';
imagejpeg($image_all, $image_url);
//imagejpeg($image_all);
imagedestroy($image_all);

$_SESSION['make-done-poster'] = $image_url;

return 'success';

/**
 * 将字符串分割为数组
 *
 * @param  string $str 字符串
 *
 * @return array       分割得到的数组
 */
function mb_str_split($str)
{
    return preg_split('/(?<!^)(?!$)/u', $str);
}