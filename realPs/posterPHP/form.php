<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
	<title>报名表</title>
	<link rel="stylesheet" href="./css/base.css">
	<link rel="stylesheet" href="./css/index.css">
</head>
<body>
	<div class="form bulid-form fs36">
		<ul class="lists flex">
            <?php
            // 所有问题
            if (isset($_GET['uid']) && isset($_GET['fid'])) {
                $x = 0;
                list($num, $array) = $sql->searchFormInformation($_GET['uid'], $_GET['fid']);
                for ($i = 0; $i < $num; $i++) {
                    list($quName, $quType, $quModes, $quBaText) = $array[$i];
                    switch ($quType) {
                        case '1':
                            // 单选
                            $modes_array = explode('|', $quModes);
                            $modes_length = $modes_array[0];
                            $modes_selects_array = explode(';', $modes_array[1]);
                            $modes_selects_count = count($modes_selects_array);
                            // 删除所有空白
                            for ($j = 0; $j < $modes_selects_count; $j++) {
                                $key = array_search('', $modes_selects_array);
                                if ($key !== false)
                                    array_splice($modes_selects_array, $key, 1);
                            }
                            $modes_selects_count = count($modes_selects_array);
                            ?>
                            <li>
                                <span class="on-name"><?= $quName ?></span>
                                <div class="radio">
                                    <?php
                                    for ($m = 0; $m < $modes_selects_count; $m++) {
                                        if ($m == 0) {
                                            ?>
                                            <span class="active">
                                        <?= $modes_selects_array[$m] ?>
                                        </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span>
                                        <?= $modes_selects_array[$m] ?>
                                        </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                            break;
                        case '2':
                            $x++;
                            // 多选
                            $modes_array = explode('|', $quModes);
                            $modes_selects_array = explode(';', $modes_array[1]);
                            array_pop($modes_selects_array);
                            $modes_length = count($modes_selects_array);
                            ?>
                            <li class="checkbox">
                                <span class="do-name"><?= $quName ?>选择</span><span>(可多选)</span>
                                <ul class="flex fs36 ab<?= $x ?>">
                                    <?php
                                    for ($n = 0; $n < $modes_length; $n++) {
                                        if ($n == 0) {
                                            ?>
                                            <li class="active"><?= $modes_selects_array[$n] ?></li>
                                            <?php
                                        } else {
                                            ?>
                                            <li><?= $modes_selects_array[$n] ?></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            break;
                        case '3' || '6':
                            // 单行文本
                            ?>
                            <li>
                                <span class="qu-name"><?= $quName ?></span>
                                <input class="iText" <?php if ($quType == '6') { echo 'onkeyup="value=value.replace(/[^\d]/g,\'\')"';} ?>type="text" value="">
                            </li>
                            <?php
                            break;
                        default :
                    }
                }
            }else{
                exit('表格不存在！');
            }
            ?>
<!--			<li class="correct">-->
<!--				<span>家长姓名</span><input type="text">-->
<!--			</li>-->
<!--			<li class="error">-->
<!--				<span>家长手机</span><input type="text">-->
<!--			</li>-->
<!--			<li>-->
<!--				<span>学生姓名</span><input type="text">-->
<!--			</li>-->
<!--			<li><span>出身日期</span><input type="text"></li>-->
<!--			<li><span>平时爱好</span><input type="text"></li>-->
<!--			<li><span>家庭住址</span><input type="text"></li>-->
<!--			<li>-->
<!--				<span>是否学过</span>-->
<!--				<div class="radio">-->
<!--					<span class="active">是</span>-->
<!--					<span>否</span>-->
<!--				</div>-->
<!--			</li>-->
<!--			<li class="checkbox">-->
<!--				<span>课程选择</span><span>(可多选)</span>-->
<!--				<ul class="flex fs36">-->
<!--					<li class="active">舞蹈课</li>-->
<!--					<li>画画课</li>-->
<!--					<li>音乐课</li>-->
<!--					<li>英语课</li>-->
<!--					<li>舞蹈课</li>					-->
<!--					<li>舞蹈课</li>-->
<!--					<li>舞蹈课</li>-->
<!--					<li>舞蹈课</li>-->
<!--				</ul>-->
<!--			</li>-->
		</ul>
		<div class="btn"><button><a href="javascript:;" onclick="upload_form(<?= $_GET['uid'] ?>, <?= $_GET['fid'] ?>)">报名</a></button></div>
	</div>
	<script src='./script/jquery.js'></script>
	<script src='./script/index.js'></script>
    <script src="./script/app.js"></script>
	<script></script>
</body>
</html>