<?php
require_once './header.php';
require_once './core/sql.php';
$sql = new SqlS();
//var_dump($_SESSION);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,user-scalable=0,maximum-scale=1.0"/>
    <title>上传二维码</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/cropper.css">
    <link rel="stylesheet" href="./css/index.css">
    <style>
        .cropper-container {
            width: 100% !important;
            height: 100% !important;
            box-sizing: border-box;
            overflow: hidden;
        }

        /* 		.cropper-canvas{
                    width: 100%!important;
                    height: auto!important;
                } */
        .cropper-bg {
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            background: none;
        }
    </style>
</head>
<body>
<div class="code">
    <div class="box">
        <form id="upload-code" method="post" action="upload.php" enctype="multipart/form-data">
            <input id="submit-code" name="image-code" type="file" value="">
        </form>
        <span>点击上传</span>
    </div>
    <div class="btn">
        <button class=""><a href="javascript:;">确认上传</a></button>
    </div>
    <div class="box1">
        <!-- <div class="box1-content"> -->
        <!-- <div class="img"><img id="targetObj"  src="./img/code.jpg" alt="二维码">
        </div> -->
        <div class="box1-content">
            <img id="image" src="./img/code.jpg">
        </div>
        <!-- </div> -->
        <ul class="bottom flex">
            <li><a href="./index.php">取消</a></li>
            <li><a href="javascript:;">还原</a></li>
            <li><a href="javascript:;" onclick="ajax_cutComplete()">完成</a></li>
        </ul>
    </div>
</div>
<script src='./script/jquery.js'></script>
<script src='./script/index.js'></script>
<script src="./script/cropper.js"></script>
<script src="./script/app.js"></script>
<script src="./script/jquery.form.js"></script>
<script>
    $('#image').cropper({
        aspectRatio: 9 / 9,
        viewMode: 1,
        dragMode: 'none',
        preview: ".small",
        responsive: false,
        restore: false,
        crop: function (e) {
            console.log(e);
        }
    });
    $(document).ready(function (e) {
        $("#submit-code").change(function () {
            ajaxSubmieImage("#upload-code", "1", "box");
        });
    });
</script>
</body>
</html>