<?php
/**
 * Created by PhpStorm.
 * User: clkj1
 * Date: 2018/7/17
 * Time: 8:54
 */

session_start();
require_once './makePoster.class.php';
$makePoster = new makePoster();

$allowedExts = array(
    "gif",
    "jpeg",
    "jpg",
    "png"
);

if (isset($_FILES['image-logo'])) {
    $rand_name = $makePoster->fileReName();
    uploadTempImage('image-logo', $rand_name, '0');
}
if (isset($_FILES['image-content1'])){
    $rand_name = $makePoster->fileReName();
    uploadTempImage('image-content1', $rand_name, '10');
}
if (isset($_FILES['image-content2'])){
    $rand_name = $makePoster->fileReName();
    uploadTempImage('image-content2', $rand_name, '10');
}
if (isset($_FILES['image-code'])){
    $rand_name = $makePoster->fileReName();
    uploadTempImage('image-code', $rand_name, '0');
}

function uploadTempImage($image_name_g, $rand_name, $s_id)
{
    require_once './core/sql.php';
    $sql = new SqlS();
    $allowedExts = array(
        "gif",
        "jpeg",
        "jpg",
        "png"
    );
    if (isset($_FILES["$image_name_g"])) {
        $image_file = $_FILES["$image_name_g"];
        $image_name = $image_file['name'];
        $image_size = $image_file['size'];
        if ($image_name != '') {
            if ($image_file['error']) {
                exit('{"status":1,"content":"' . $image_file['error'] . '"}');
            }
            if ($image_size > (round(1024 * 1024, 2) * 12)) { //限制上传大小
                exit('{"status":1,"content":"图片大小不能超过12M"}');
            }
            // 限制上传格式
            $temp_image_name_array = explode('.', $image_name);
            $ff_image = end($temp_image_name_array);
            if (!in_array($ff_image, $allowedExts)) {
                exit('{"status":2,"content":"图片格式不对！"}');
            }
            $image_filename = $rand_name . '.' . $ff_image; //命名图片名称
            //上传路径
            $image_path = './uploads/' . $image_filename;
            $_SESSION["$image_name_g"] = $image_path;
            move_uploaded_file($image_file['tmp_name'], $image_path);
            $size_image = round($image_size / 1024, 2); //转换成kb
            $image_im_size = getimagesize($image_path);
            $sql->addTimeOutImages($_SESSION['uid'], $image_path);
            exit('{"status":'.$s_id.',"url":"' . $image_path . '","width":"' . $image_im_size[0] . '","height":"' . $image_im_size[1] . '","size":"' . $size_image . '"}');
        }
    }
}