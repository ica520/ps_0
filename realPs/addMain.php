<?php
/**
 * Created by PhpStorm.
 * User: CaiJianling
 * Date: 2018/7/8
 * Time: 20:59
 */


const ADX = 28.346;

//创建背景：

$width = 1080;
$height = 1829;
$image_all = imagecreatetruecolor($width, $height);

//白色背景
$white = imagecolorallocate($image_all, 255, 255, 255);
$tsc = imagecolorallocatealpha($image_all, 255, 255, 255, 127);

imagefilledrectangle($image_all, 0, 0, $width, $height, $white);

//背景图片
$bg_array = array('images/bg1.png', 'images/bg2.png', 'images/bg3.png');
$image_bg = imagecreatefrompng($bg_array[mt_rand(0, 2)]);

//加上背景图片
imagecopy($image_all, $image_bg, 0, 0, 0, 0, $width, $height);

//加上logo背景

$image_logo_bg = imagecreatefrompng('images/logobg.png');

$image_logo_bg_size = getimagesize('images/logobg.png');

$image_logo_bg_copy_width = 44;
$image_logo_bg_copy_height = -8;
imagecopy($image_all, $image_logo_bg, $image_logo_bg_copy_width, $image_logo_bg_copy_height, 0, 0, $image_logo_bg_size[0], $image_logo_bg_size[1]);

//获取用户logo：
//$image_logo = imagecreatefrompng('');
//imagecopy($image_all, $image_logo_bg, 50, 0, 0, 0, )

//添加标题图片
$title_array = array('images/title1.png', 'images/title2.png', 'images/title3.png');
$set_title = mt_rand(0, 2);
$image_big_title = imagecreatefrompng($title_array[$set_title]);
$image_big_title_size = getimagesize($title_array[$set_title]);

switch ($set_title) {
    case 0:
        $image_big_title_copy_width = 111;
        $image_big_title_copy_height = 56;
        break;
    case 1:
        $image_big_title_copy_width = 186;
        $image_big_title_copy_height = -68;
        break;
    case 2:
        $image_big_title_copy_width = 212;
        $image_big_title_copy_height = 133;
        break;
}


imagecopy($image_all, $image_big_title, $image_big_title_copy_width, $image_big_title_copy_height, 0, 0, $image_big_title_size[0], $image_big_title_size[1]);

//添加中心两张图片
//大的：
$image_c1bg1 = imagecreatefrompng('images/c1bg1.png');
$image_c1bg1_size = getimagesize('images/c1bg1.png');

$image_c1bg1_copy_width = 75;
$image_c1bg1_copy_height = 850;
imagecopy($image_all, $image_c1bg1, $image_c1bg1_copy_width, $image_c1bg1_copy_height, 0, 0, $image_c1bg1_size[0], $image_c1bg1_size[1]);

$image_c1bg2 = imagecreatefrompng('images/c1bg2.png');
$image_c1bg2_size = getimagesize('images/c1bg2.png');

$image_c1bg2_copy_width = 110;
$image_c1bg2_copy_height = 890;
imagecopy($image_all, $image_c1bg2, $image_c1bg2_copy_width, $image_c1bg2_copy_height, 0, 0, $image_c1bg2_size[0], $image_c1bg2_size[1]);

//照片|816x434
$file_pic1_string = 'images/xly3.jpeg';
list($pic1_width, $pic1_height, $pic1_type) = getimagesize($file_pic1_string);
switch ($pic1_type) {
    case 1:
        $image_pic1 = imagecreatefromgif($file_pic1_string);
        break;
    case 2:
        $image_pic1 = imagecreatefromjpeg($file_pic1_string);
        break;
    case 3:
        $image_pic1 = imagecreatefrompng($file_pic1_string);
        break;
    default :
        exit("fail img $file_pic1_string");
}
$pic1_bg = imagecreatetruecolor($width, $height);
imagefill($pic1_bg, 0, 0, $tsc);
imagecopyresampled($pic1_bg, $image_pic1, 0, 0, 0, 0, 816, 434, $pic1_width, $pic1_height);
$image_pic1_rotate = imagerotate($pic1_bg, 1.51, $tsc, 1);
imagecopy($image_all, $image_pic1_rotate, 110, 885, 0, 0, $width, $height);


//小的
$image_c2bg1 = imagecreatefrompng('images/c2bg1.png');
$image_c2bg1_size = getimagesize('images/c2bg1.png');

$image_c2bg1_copy_width = 752;
$image_c2bg1_copy_height = 1180;
imagecopy($image_all, $image_c2bg1, $image_c2bg1_copy_width, $image_c2bg1_copy_height, 0, 0, $image_c2bg1_size[0], $image_c2bg1_size[1]);

$image_c2bg2 = imagecreatefrompng('images/c2bg2.png');
$image_c2bg2_size = getimagesize('images/c2bg2.png');

$image_c2bg2_copy_width = 780;
$image_c2bg2_copy_height = 1200;
imagecopy($image_all, $image_c2bg2, $image_c2bg2_copy_width, $image_c2bg2_copy_height, 0, 0, $image_c2bg2_size[0], $image_c2bg2_size[1]);

//照片|218x170
$file_pic1_string = 'images/xly2.jpeg';
list($pic1_width, $pic1_height, $pic1_type) = getimagesize($file_pic1_string);
switch ($pic1_type) {
    case 1:
        $image_pic1 = imagecreatefromgif($file_pic1_string);
        break;
    case 2:
        $image_pic1 = imagecreatefromjpeg($file_pic1_string);
        break;
    case 3:
        $image_pic1 = imagecreatefrompng($file_pic1_string);
        break;
    default :
        exit("fail img $file_pic1_string");
}
$pic1_bg = imagecreatetruecolor($width, $height);
imagefill($pic1_bg, 0, 0, $tsc);
imagecopyresampled($pic1_bg, $image_pic1, 0, 0, 0, 0, 218, 170, $pic1_width, $pic1_height);
$image_pic1_rotate = imagerotate($pic1_bg, -5.77, $tsc, 1);
imagecopy($image_all, $image_pic1_rotate, 610, 1200, 0, 0, $width, $height);


//二维码:
$image_qrcode = imagecreatefrompng('images/qrcode2.png');
$image_qrcode_size = getimagesize('images/qrcode2.png');

$image_qrcode_copy_width = 844;
$image_qrcode_copy_height = 1477;
imagecopy($image_all, $image_qrcode, $image_qrcode_copy_width, $image_qrcode_copy_height, 0, 0, $image_qrcode_size[0], $image_qrcode_size[1]);

//微信扫码 了解更多详情
$wxsm_x = 845;
$wxsm_y = 1705;

$gray = imagecolorallocatealpha($image_all, 107, 107, 107, 0);

$fonts = array(
    '等线常规' => __DIR__ . '/fonts/Deng.ttf',
    '等线粗体' => __DIR__ . '/fonts/Dengb.ttf',
    '等线细体' => __DIR__ . '/fonts/Dengl.ttf'
);
imagettftext($image_all, 14, 0, $wxsm_x, $wxsm_y, $gray, $fonts['等线常规'], '微信扫码 了解更多详情');

//招生对象等：
//招生对象：内容内容内容内容内容内容 咨询热线：0574-87993919/87918150 地址：宁波市海曙区丽园北路668号
$black = imagecolorallocatealpha($image_all, 0, 0, 0, 0);
imagettftext($image_all, 22, 0, 186, 1520, $black, $fonts['等线粗体'], '招生对象：');
imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520, $black, $fonts['等线常规'], '内容内容内容内容内容内容');
imagettftext($image_all, 22, 0, 186, 1520 + 22 * 3, $black, $fonts['等线粗体'], '咨询热线：');
imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 3, $black, $fonts['等线常规'], '0574-87993919/87918150');
imagettftext($image_all, 22, 0, 186, 1520 + 22 * 6, $black, $fonts['等线粗体'], '地       址：');
imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 6, $black, $fonts['等线常规'], '宁波市海曙区丽园北路668号');
imagettftext($image_all, 22, 0, 186 + 22 * 7, 1520 + 22 * 8, $black, $fonts['等线常规'], '        美丽园大厦 602');

//内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，
//内容介绍，内容介绍，内容介绍，
$content = '内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，内容介绍，';

$content_array = mb_str_split($content);
$count_content = count($content_array);

$content_floor = floor($count_content);
for ($j = 0; $j < ($count_content / 35); $j++) {
    if ($count_content - $j * 35 > 35) {
        for ($i = 0; $i < 35; $i++) {
            $text_position_x = 115 + 24 * $i;
            $text_position_y = 585 + 35 * $j;
            imagettftext($image_all, 19, 0, $text_position_x, $text_position_y, $black, $fonts['等线常规'], $content_array[($j * 35 + $i)]);
        }
    } else {
        for ($i = 0; $i < ($count_content - $j * 35); $i++) {
            $msi = $count_content % 35;
            $text_position_x = 115 + 24 * $i;
            $text_position_x = $text_position_x + (35 / 2 * 24 - $msi / 2 * 24);
            $text_position_y = 585 + 35 * $j;
            imagettftext($image_all, 19, 0, $text_position_x, $text_position_y, $black, $fonts['等线常规'], $content_array[($j * 35 + $i)]);
        }
    }
}


//imagettftext( $image_all, 19, 0, 115, 585, $black, $fonts['等线常规'], $content );


//底部横条
$image_csbar = imagecreatefrompng('images/csbar.png');
$image_csbar_size = getimagesize('images/csbar.png');
$image_logo_csbar_copy_width = 220;
$image_logo_csbar_copy_height = 1428;
imagecopy($image_all, $image_csbar, $image_logo_csbar_copy_width, $image_logo_csbar_copy_height, 0, 0, $image_csbar_size[0], $image_csbar_size[1]);

//暑期活动课程888元/12节课时
//团购价588元/12节课时（五人成团）
$red_deep = imagecolorallocatealpha($image_all, 213, 23, 23, 0);
imagettftext($image_all, 30, 0, 218, 750, $black, $fonts['等线粗体'], '暑期活动课程');
$ontcount1 = count(mb_str_split('暑期活动课程'));
imagettftext($image_all, 45, 0, 218 + 30 * 1.35 * $ontcount1, 750, $red_deep, $fonts['等线粗体'], ' 888');//465
$ontcount2 = count(mb_str_split(' 888'));
imagettftext($image_all, 30, 0, 218 + 30 * 1.35 * $ontcount1 + 45 * 0.75 * $ontcount2, 750, $black, $fonts['等线粗体'], '元/12节课时');//590
imagettftext($image_all, 30, 0, 218, 830, $black, $fonts['等线粗体'], '团购价');
imagettftext($image_all, 45, 0, 340, 830, $red_deep, $fonts['等线粗体'], '588');
imagettftext($image_all, 30, 0, 460, 830, $black, $fonts['等线粗体'], '元/12节课时（五人成团）');

//红球
$image_ball = imagecreatefrompng('images/ball.png');
$image_ball_size = getimagesize('images/ball.png');
imagecopy($image_all, $image_ball, 170, 725, 0, 0, $image_ball_size[0], $image_ball_size[1]);
imagecopy($image_all, $image_ball, 170, 805, 0, 0, $image_ball_size[0], $image_ball_size[1]);

//logo图片：
$logo_size = getimagesize('images/Logo1.png');
$image_logo = imagecreatefrompng('images/Logo1.png');
imagecopy($image_all, $image_logo, 10, 20, 0, 0, $logo_size[0], $logo_size[1]);


//header("content-type:text/html;charset=utf-8");
mb_internal_encoding('UTF-8');
header('Content-Type:image/png');
imagepng($image_all);
imagedestroy($image_all);


/**
 * 将字符串分割为数组
 *
 * @param  string $str 字符串
 *
 * @return array       分割得到的数组
 */
function mb_str_split($str)
{
    return preg_split('/(?<!^)(?!$)/u', $str);
}